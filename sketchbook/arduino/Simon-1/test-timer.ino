#include <time.h>

//----------------------------------
// Pins association
//----------------------------------

// Digital Input 
const int Dig_In_Pin_1                = PC3;  // Up button
const int Dig_In_Pin_2                = PC5;  // Down Button 
const int Dig_In_Pin_3                = PC4;  // Start-Stop button

// Digital Output
const int Dig_Out_Blink_Pin_1         = PB4;  // LED 1
const int Dig_Out_Blink_Pin_2         = PB5;  // LED 2
const int Dig_Out_Heater_Relay_Pin_1  = PC6;  // Heater Relay 1
const int Dig_Out_Heater_Relay_Pin_2  = PD3;  // Heater Relay 2

// Display
const int Dig_Out_Disp_Pin_1          = PA1; // CLK
const int Dig_Out_Disp_Pin_2          = PA2; // STB
const int Dig_Out_Disp_Pin_3          = PA3; // DIO


//----------------------------------
// Global Variables
//----------------------------------

// States, Rates and Counts
unsigned int Dig_In_1       = HIGH;
unsigned int Dig_In_2       = HIGH;
unsigned int Dig_In_3       = HIGH;

unsigned int Dig_In_Prev_1  = HIGH;
unsigned int Dig_In_Prev_2  = HIGH;
unsigned int Dig_In_Prev_3  = HIGH;

unsigned int Dig_In_1_Count = 0;
unsigned int Dig_In_2_Count = 0;
unsigned int Dig_In_3_Count = 0;

unsigned int Dig_In_1_Rate  = 200;
unsigned int Dig_In_2_Rate  = 200;   
unsigned int Dig_In_3_Rate  = 100;    

unsigned int Dig_In_3_State = false;

unsigned int blinkState     = 0;

// Schedule Timing
unsigned long now               =  0;
unsigned long start             =  0;

unsigned long lastDigInHop      =  0;
unsigned long lastDigOutHop     =  0;
unsigned long lastSerialInHop   =  0;
unsigned long lastSerialOutHop  =  0;
unsigned long lastTimerHop      =  0;
unsigned long lastBlinkHop      =  0;

unsigned long durationDigIn     = 0.02  * 1000000;
unsigned long durationDigOut    = 0.1   * 1000000;
unsigned long durationSerialIn  = 0.1   * 1000000;
unsigned long durationSerialOut = 1.0   * 1000000;
unsigned long durationTimer     = 1.0   * 1000000;
unsigned long durationBlink     = 0.5   * 1000000;

// Timer Status
unsigned long timerStartTime    =  0;
unsigned long timerStopTime     =  0;
unsigned long timerFullTime     = 10;
unsigned int  timerState        = 0;
unsigned int  timerState2       = 0;

// Serial processing
char   inCharArray[32];
char   outCharArray[32];

// Decriment the heater ontime
void processTimerUpEvent(){
  if (timerFullTime <= (30 * 60))
    timerFullTime++;
}

// Incriment the heater ontime
void processTimerDownEvent(){
  if (timerFullTime >= 1)
    timerFullTime--;
}

// Start the Heater
void processTimerStartEvent(){
  if (timerState == 0){
    if (timerFullTime > 0){
      Serial_print_s("Starting Timer. \r\n");
      timerState = 1;
      timerStartTime = now;
    }
    else{
      Serial_print_s("NOT Starting Timer. Need to set time! \r\n");
    }
  }
  else if (timerState == 1){
    Serial_print_s("Stopping Timer. \r\n");
    timerState = 0; 
    timerStopTime = now;
  }
}

// Process Serial Input
void processSerialInEvent() {
  while (Serial_available() > 0) {
    // read incoming serial data:
    char inChar = Serial_read();
    delay(10);
    if (inChar == '\n'){
      Serial_print_s("CR received! \r\n");
    }
    else{
      Serial_print_s("Received:["); Serial_print_c(inChar); Serial_print_s("] \r\n");
      if (inChar == '1'){
        Serial_print_s("Command:[1-UP] \r\n");
        processTimerUpEvent();
      }
      else if (inChar == '2'){
        Serial_print_s("Command:[2-DOWN] \r\n");
        processTimerDownEvent();
      }
      else if (inChar == '3'){
        Serial_print_s("Command:[3-START] \r\n");
        processTimerStartEvent();
      }
      else{
        Serial_print_s("Command:["); Serial_print_c(inChar); Serial_print_s("-UNKNOWN!] \r\n");
      }
    }
  }
}

// Output some serial ststus
void processSerialOutEvent() {
  // Output some serial info

  Serial_print_s("Up Time:");
  sprintf(outCharArray, "%02ld:%02ld", (((now - start) / 1000000) / 60), (((now - start) / 1000000) % 60));
  Serial_print_s(outCharArray);
  Serial_print_s("   ");

  Serial_print_s("Heater Time:");
  sprintf(outCharArray, "%02ld:%02ld", (timerFullTime / 60), (timerFullTime % 60));
  Serial_print_s(outCharArray);
  Serial_print_s("   ");

  Serial_print_s("Heater:");
  if (timerState == 1)
    Serial_print_s("[ON] ");
  else
    Serial_print_s("[OFF]");
  Serial_print_s("   ");
    
  Serial_print_s(" \r\n");

  now = micros();
}

// Read the state of the touch buttons
void processDigInEvent() {
  // Store the old values for comparison
  Dig_In_Prev_1 = Dig_In_1;
  Dig_In_Prev_2 = Dig_In_2;
  Dig_In_Prev_3 = Dig_In_3;

  // Read Digital input Pins
  Dig_In_1 = digitalRead(Dig_In_Pin_1);
  Dig_In_2 = digitalRead(Dig_In_Pin_2);
  Dig_In_3 = digitalRead(Dig_In_Pin_3);

  // Button 1 Timer-UP
  if (Dig_In_1 == LOW){
    if (Dig_In_Prev_1 == HIGH){
//      Serial_print_s("Button:[1-UP] PRESS\r\n");
      processTimerUpEvent();
    }      
    Dig_In_1_Count++;
    if (Dig_In_1_Count >= Dig_In_1_Rate){
//      Serial_print_s("Button:[1-UP] HOLD\r\n");
      processTimerUpEvent();
      Dig_In_1_Count = 0;
      if (Dig_In_1_Rate > 0){
        Dig_In_1_Rate--;
      }
    }
  }
  else{
    Dig_In_1_Count = 0;
    Dig_In_1_Rate = 10;
  }
  
  // Button 2 Timer-DOWN
  if (Dig_In_2 == LOW){
    if (Dig_In_Prev_2 == HIGH){
//      Serial_print_s("Button:[2-DOWN] PRESS\r\n");
      processTimerDownEvent();
    }      
    Dig_In_2_Count++;
    if (Dig_In_2_Count >= Dig_In_2_Rate){
//      Serial_print_s("Button:[2-DOWN] HOLD\r\n");
      processTimerDownEvent();
      Dig_In_2_Count = 0;
      if (Dig_In_2_Rate > 0){
        Dig_In_2_Rate--;
      }
    }
  }
  else{
    Dig_In_2_Count = 0;
    Dig_In_2_Rate = 10;
  }

  // Button 3 Timer-STOP-START
  if (Dig_In_3 == LOW){
    Dig_In_3_Count++;
    if ((Dig_In_3_Count >= Dig_In_3_Rate) && (Dig_In_3_State == false)){
//      Serial_print_s("Button:[3-START-STOP] HOLD\r\n");
      Dig_In_3_State = true;
      processTimerStartEvent();
      Dig_In_3_Count = 0;
      if (Dig_In_3_Rate > 0){
        Dig_In_3_Rate--;
      }
    }
  }
  else{
    Dig_In_3_State = false;
    Dig_In_3_Count = 0;
    Dig_In_3_Rate = 10;
  }

}

// Write DIO {turn relays ON / OFF}
void processDigOutEvent() {
  digitalWrite(Dig_Out_Heater_Relay_Pin_1, timerState);
  digitalWrite(Dig_Out_Heater_Relay_Pin_2, timerState2);
}

// Blink the 2 LEDs
void processBlinkEvent(){
  if (blinkState==HIGH)
    blinkState = LOW;
  else
    blinkState = HIGH;
  digitalWrite(Dig_Out_Blink_Pin_1, blinkState);
  digitalWrite(Dig_Out_Blink_Pin_2, !blinkState);
}

// Update timer status'
void processTimerEvent() {
// Turn the first relay OFF if the timer has run down
  if (timerState == 1){
    timerFullTime--;
    if (timerFullTime <= 0){
      timerState = 0;
      timerStopTime = now;
    }
  }

// Turn the second relay ON or OFF one second after the first relay
  if (timerState == 1){
    if (now - timerStartTime > 1000000)
      timerState2 = 1;
  }
  else{
    if (now - timerStopTime > 1000000)
      timerState2 = 0;
  }
  
}

// Write the Pinout info to the serial console
void serialOutputPinOuts(){
  Serial_print_s("PD4:");  sprintf(outCharArray, "%02d \r\n", PD4);  Serial_print_s(outCharArray);
  Serial_print_s("PD5:");  sprintf(outCharArray, "%02d \r\n", PD5);  Serial_print_s(outCharArray);
  Serial_print_s("PD6:");  sprintf(outCharArray, "%02d \r\n", PD6);  Serial_print_s(outCharArray);
  Serial_print_s("NRST: \r\n");
  Serial_print_s("PA1:");  sprintf(outCharArray, "%02d \r\n", PA1);  Serial_print_s(outCharArray);
  Serial_print_s("PA2:");  sprintf(outCharArray, "%02d \r\n", PA2);  Serial_print_s(outCharArray);
  Serial_print_s("GND: \r\n");
  Serial_print_s("VCAP: \r\n");
  Serial_print_s("VDD: \r\n");
  Serial_print_s("PA3:");  sprintf(outCharArray, "%02d \r\n", PA3);  Serial_print_s(outCharArray);

  Serial_print_s("PB5:");  sprintf(outCharArray, "%02d \r\n", PB5);  Serial_print_s(outCharArray);
  Serial_print_s("PB4:");  sprintf(outCharArray, "%02d \r\n", PB4);  Serial_print_s(outCharArray);
  Serial_print_s("PC3:");  sprintf(outCharArray, "%02d \r\n", PC3);  Serial_print_s(outCharArray);
  Serial_print_s("PC4:");  sprintf(outCharArray, "%02d \r\n", PC4);  Serial_print_s(outCharArray);
  Serial_print_s("PC5:");  sprintf(outCharArray, "%02d \r\n", PC5);  Serial_print_s(outCharArray);
  Serial_print_s("PC6:");  sprintf(outCharArray, "%02d \r\n", PC6);  Serial_print_s(outCharArray);
  Serial_print_s("PC7:");  sprintf(outCharArray, "%02d \r\n", PC7);  Serial_print_s(outCharArray);
  Serial_print_s("PD1:");  sprintf(outCharArray, "%02d \r\n", PD1);  Serial_print_s(outCharArray);
  Serial_print_s("PD2:");  sprintf(outCharArray, "%02d \r\n", PD2);  Serial_print_s(outCharArray);
  Serial_print_s("PD3:");  sprintf(outCharArray, "%02d \r\n", PD3);  Serial_print_s(outCharArray);
}

void setup() {  
  // Initialize the Serial port
  Serial_begin(9600);
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s("-------------------------------------------------------------------- \r\n");
  delay(50);
  Serial_print_s("Begin. \r\n");
  delay(50);
  Serial_print_s("1 = [TIMER-UP]    2 = [TIMER-DOWN]    3 = [START / STOP] \r\n");
  delay(50);
  Serial_print_s("-------------------------------------------------------------------- \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(500);

  pinMode(Dig_In_Pin_1, INPUT_PULLUP);
  pinMode(Dig_In_Pin_2, INPUT_PULLUP);
  pinMode(Dig_In_Pin_3, INPUT_PULLUP);

  pinMode(Dig_Out_Blink_Pin_1, OUTPUT);
  pinMode(Dig_Out_Blink_Pin_2, OUTPUT);

  pinMode(Dig_Out_Heater_Relay_Pin_1, OUTPUT);
  pinMode(Dig_Out_Heater_Relay_Pin_2, OUTPUT);

  // Get the start time
  start = micros();

  now = micros();
  start            = now;
  lastDigInHop     = now;
  lastDigOutHop    = now;
  lastSerialInHop  = now;
  lastSerialOutHop = now;
  lastTimerHop     = now;
  lastBlinkHop     = now;
}

void loop() {  
  if (now - lastTimerHop >= durationTimer){
    lastTimerHop = now;
    processTimerEvent();
  }
  if (now - lastSerialOutHop >= durationSerialOut){
    lastSerialOutHop = now;
    processSerialOutEvent();
  }
  if (now - lastSerialInHop >= durationSerialIn){
    lastSerialInHop = now;
    processSerialInEvent();
  }
  if (now - lastDigInHop >= durationDigIn){
    lastDigInHop = now;
    processDigInEvent();
  }
  if (now - lastDigOutHop >= durationDigOut){
    lastDigOutHop = now;
    processDigOutEvent();
  }
  if (now - lastBlinkHop >= durationBlink){
    lastBlinkHop = now;
    processBlinkEvent();
  }

  now = micros();
}
