/*
 * Heater Controller.
 * Simbop 29/11/2020.
 * Takes input from three buttons (START UP DOWN).
 * These are used to set the time UP and DOWN and to START ans STOP the timer.
 * Displays the set time on a 4 digit display.
 * While the timer is active, drives relays which in turn drive the heaters.
 */
 
#include <time.h>

//----------------------------------
// Pins mapping
//----------------------------------

// Digital Input 
const unsigned int DigInPin1               = PC3;  // Up button
const unsigned int DigInPin2               = PC5;  // Down Button 
const unsigned int DigInPin3               = PC4;  // Start-Stop button

// Digital Output
const unsigned int DigOutBlinkPin1         = PB4;  // LED 1
const unsigned int DigOutBlinkPin2         = PB5;  // LED 2
const unsigned int DigOutHeaterRelayPin1   = PC6;  // Heater Relay 1
const unsigned int DigOutHeaterRelayPin2   = PD3;  // Heater Relay 2
const unsigned int DigOutBuzzerPin         = PD4;  // Buzzer

// Display
const unsigned int DigOutDispPin1          = PA1; // CLK
const unsigned int DigOutDispPin2          = PA2; // STB
const unsigned int DigOutDispPin3          = PA3; // DIO


//----------------------------------
// Global Variables
//----------------------------------

// States, Rates and Counts
unsigned int  DigIn1             = LOW;
unsigned int  DigIn2             = LOW;
unsigned int  DigIn3             = LOW;

unsigned int  DigInPrev1         = LOW;
unsigned int  DigInPrev2         = LOW;
unsigned int  DigInPrev3         = LOW;

unsigned int  DigInCount1        = 0;
unsigned int  DigInCount2        = 0;
unsigned int  DigIn3_Count       = 0;

unsigned int  DigInRateMax       = 40;
unsigned int  DigInRate1         = 40;
unsigned int  DigInRate2         = 40;
unsigned int  DigInRate3         = 40;

unsigned int  DigInMod           = 1;
unsigned int  DigInModMin        = 1;
unsigned int  DigInModMax        = 4;
unsigned long DigInModCount      = 0;
unsigned long DigInModCountMax   = 100 * 1000000;
unsigned int  DigInState3        = false;
unsigned int  blinkState         = HIGH;

// Schedule Timing
unsigned long now                =  0;
unsigned long lastDigInHop       =  0;
unsigned long lastDigOutHop      =  0;
unsigned long lastSerialOutHop   =  0;
unsigned long lastTimerHop       =  0;
unsigned long lastDisplayHop     =  0;
unsigned long lastBlinkHop       =  0;

unsigned long durationDigIn      = 0.01  * 1000000;
unsigned long durationDigOut     = 0.2   * 1000000;
unsigned long durationSerialOut  = 1.0   * 1000000;
unsigned long durationTimer      = 1.0   * 1000000;
unsigned long durationDisplay    = 1.0   * 1000000;
unsigned long durationBlink      = 1.0   * 1000000;

// Timer / Heater State
unsigned long timerStartTime     = 0;
unsigned long timerStopTime      = 0;
unsigned long timerFullTime      = 0;
unsigned long timerFullTimeBck   = 0;
unsigned int  timerState         = 0;
unsigned int  timerState2        = 0;

// Display
const unsigned int strobe         = PA2;
const unsigned int clock          = PA1;
const unsigned int data           = PA3;

// Digit address definitions
//                                      1     2     3     4
const unsigned int digA[4]        = {0xc0, 0xc2, 0xc4, 0xc6}; // Digit address part 1
const unsigned int digB[4]        = {0xc1, 0xc3, 0xc5, 0xc7}; // Digit address part 2

// Digit Segment definitions
//                                      0     1     2     3     4     5     6     7     8     9     0     F     _
const unsigned int segA[12]       = {0xfe, 0x02, 0x1d, 0x57, 0x03, 0x57, 0x1f, 0x72, 0xff, 0x57, 0x1e, 0x19, 0x00}; // Digit segment definition part 1
const unsigned int segB[12]       = {0xfe, 0xF3, 0xf7, 0xf7, 0xf9, 0xed, 0xe8, 0xf2, 0xff, 0xfd, 0x1e, 0x0e, 0x00}; // Digit segment definition part 2

unsigned long displayTimeout      = 10 * 1000000;
unsigned long displayTimeoutTime  = 10 * 1000000;
boolean       displayState        = false;

// String formatting
char   charArray[8];

// Beep
void beep(unsigned long beepTime){
  for (unsigned long a=0;a < beepTime; a++){
    for (unsigned long b=0;b < 50; b++){
       digitalWrite(DigOutBuzzerPin, LOW);
       for (int c = 0; c < 5;c++);
       digitalWrite(DigOutBuzzerPin, HIGH);
//       for (int c = 0; c < 5;c++);
    }
  }
}

void beepShort(){
  beep(3);
}

void beepMid(){
  beep(150);
}

void beepLong(){
  beep(1000);
}

// Talk to display
void displaySendCommand(uint8_t value){
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, value);
  digitalWrite(strobe, HIGH);
}

// Reset the display
void displayReset(){
  displaySendCommand(0x40);                 // set auto increment mode
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, 0xc0);    // set starting address to 0
  for(uint8_t i = 0; i < 16; i++)
  {
    shiftOut(data, clock, LSBFIRST, 0x00);
  }
  digitalWrite(strobe, HIGH);
}

// Start the display
void displayOn(){
  if (displayState == false){
    for (int a = 40;a > 0;a--){
      displaySendCommand(a);
    }
    displaySendCommand(0x8f);  // activate
    displayReset();
    displayState = true;
    beepShort();
  }
}

// Stop the display
void displayOff(){
  displaySendCommand(0x7f);
  displayState = false;
}

// Update a digit on the display
void displayUpdateDigit(unsigned int location, unsigned int digit){
  displaySendCommand(0x44);         // set single address
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, digA[location]);
  shiftOut(data, clock, LSBFIRST, segA[digit]);
  digitalWrite(strobe, HIGH);
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, digB[location]);
  shiftOut(data, clock, LSBFIRST, segB[digit]);
  digitalWrite(strobe, HIGH);
//  digitalWrite(PB4, HIGH);
//  digitalWrite(PB5, LOW);
}

// Process Display Update event
void processDisplayUpdateEvent(){
  if (now - displayTimeout > displayTimeoutTime){
    // Turn the display off
    displayOff();
  }
  else{
    displayOn();                      // Turn on the display

    if (timerState == true) { 
      if  (blinkState == true){
        displaySendCommand(29);       // Turn off dot dot    (This mode is slightly dimmer - Needs to be addressed)
      }  
      else{
        displaySendCommand(32);       // Turn on dot dot     (This mode is slightly brighter - Needs to be addressed)
      }
      displaySendCommand(0x8f);       // activate
      displaySendCommand(0x44);       // set single address
    }
    else{
      displaySendCommand(32);         // Turn off dot dot
      displaySendCommand(0x8f);       // activate
      displaySendCommand(0x44);       // set single address
    }    

    // Update the display
    displayUpdateDigit(0, (timerFullTime / 60 / 10) );
    displayUpdateDigit(1, (timerFullTime / 60 % 10) );
    displayUpdateDigit(2, (timerFullTime % 60 / 10) );
    displayUpdateDigit(3, (timerFullTime % 60 % 10) );
  }
}

// Decriment the heater runtime event
void processTimerUpEvent(){
  if (timerFullTime <= (30 * 60 - DigInMod))
    timerFullTime+=DigInMod;
  else
    timerFullTime = (30 * 60);

  processDisplayUpdateEvent();
}

// Incriment the heater runtime event
void processTimerDownEvent(){
  if (timerFullTime >= DigInMod)
    timerFullTime-=DigInMod;
  else
    timerFullTime = 0;

  processDisplayUpdateEvent();
}

// Start the Heater Event
void processTimerStartEvent(){
  if (timerState == 0){
    if (timerFullTime > 0){
      Serial_print_s("Starting Timer. \r\n");
      timerState = 1;
      timerFullTimeBck = timerFullTime;
      timerStartTime = now;
    }
    else{
      Serial_print_s("NOT Starting Timer. Need to set time! \r\n");
    }
  }
  else if (timerState == 1){
    Serial_print_s("Stopping Timer. \r\n");
    timerState = 0; 
    timerStopTime = now;
  }
}

// Output some serial ststus
void processSerialOutEvent() {
  sprintf(charArray, "%02ld:%02ld", (timerFullTime / 60), (timerFullTime % 60));
  Serial_print_s(charArray);
  Serial_print_s("   ");

  Serial_print_s("Ht:");
  if (timerState == 1)
    Serial_print_s("[ON] ");
  else
    Serial_print_s("[OFF]");
  Serial_print_s("   ");
    
  Serial_print_s(" \r\n");

  now = micros();
}

// Read the state of the touch buttons
void processDigInEvent() {
  // Store the old values for comparison
  DigInPrev1 = DigIn1;
  DigInPrev2 = DigIn2;
  DigInPrev3 = DigIn3;

  // Read Digital input Pins
  DigIn1 = digitalRead(DigInPin1);
  DigIn2 = digitalRead(DigInPin2);
  DigIn3 = digitalRead(DigInPin3);

  // Button 1 Timer-UP
  if ( (DigIn1 == LOW) && (timerState == 0) ){
    displayTimeout = now;
    if (DigInPrev1 == HIGH){                          // Button pressed
//      Serial_print_s("Button:[1-UP] PRESS\r\n");
      beepShort();
      DigInMod = DigInModMin;
      processTimerUpEvent();
    }      
    DigInCount1++;
    if (DigInCount1 >= DigInRate1){                   // Button held
//      Serial_print_s("Button:[1-UP] HOLD\r\n");
      beepShort();
      processTimerUpEvent();
      DigInCount1 = 0;

      if (DigInRate1 > 0){                            // Increase size of change
        DigInRate1--;
        if (DigInRate1 < (DigInRateMax / 2)){
          DigInMod = DigInModMax / 2;
        }
        else{
          DigInMod = DigInModMin;
        }
      }
      else{
        if (DigInModCount > DigInModCountMax){        // Increase rate change size
          if (timerFullTime <= (30 * 60 - (DigInModMax+1))){
            if (DigInMod < DigInModMax){
              DigInMod++;
              DigInModCount = 0;
            }
          }
          else{
            DigInMod = DigInModMin;
          }
        }
        else{
          DigInModCount++;
        }

      }
    }
  }
  else{
    DigInCount1 = 0;
    DigInRate1 = DigInRateMax;
  }
  
  // Button 2 Timer-DOWN
  if ( (DigIn2 == LOW) && (timerState == 0) ){
    displayTimeout = now;
    if (DigInPrev2 == HIGH){                          // Button pressed
//      Serial_print_s("Button:[2-DOWN] PRESS\r\n");
      beepShort();
      DigInMod = DigInModMin;
      processTimerDownEvent();
    }      
    DigInCount2++;
    if (DigInCount2 >= DigInRate2){                   // Button held
//      Serial_print_s("Button:[2-DOWN] HOLD\r\n");
      beepShort();
      processTimerDownEvent();
      DigInCount2 = 0;

      if (DigInRate2 > 0){                            // Increase size of change
        DigInRate2--;
        if (DigInRate2 < (DigInRateMax / 2)){
          DigInMod = DigInModMax / 2;
        }
        else{
          DigInMod = DigInModMin;
        }
      }
      else{
        if (DigInModCount > DigInModCountMax){        // Increase rate of change
          if (timerFullTime >= (DigInModMax+1)){
            if (DigInMod < DigInModMax){
              DigInMod++;
              DigInModCount = 0;
            }
          }
          else{
            DigInMod = DigInModMin;
          }
        }
        else{
          DigInModCount++;
        }
      }

    }
  }
  else{
    DigInCount2 = 0;
    DigInRate2 = DigInRateMax;
  }

  // Button 3 Timer-STOP-START
  if (DigIn3 == LOW){
    displayTimeout = now;
    DigIn3_Count++;
    
    if (DigIn3_Count == 1){                                         // Button Press
      beepShort();
    }
    
    if ((DigIn3_Count >= DigInRate3) && (DigInState3 == false)){    // Button Hold
//      Serial_print_s("Button:[3-START-STOP] HOLD\r\n");
      DigInState3 = true;
      processTimerStartEvent();
      DigIn3_Count = 0;
      if (DigInRate3 > 0){
        DigInRate3--;
      }
      beepMid();
    }
  }
  else{
    DigInState3 = false;
    DigIn3_Count = 0;
    DigInRate3 = DigInRateMax;
  }

}

// Write DIO {turn relays ON / OFF}
void processDigOutEvent() {
  digitalWrite(DigOutHeaterRelayPin1, timerState);
  digitalWrite(DigOutHeaterRelayPin2, timerState2);
}

// Blink the 2 LEDs
void processBlinkEvent(){
  blinkState = !blinkState;
//  digitalWrite(DigOutBlinkPin1, blinkState);
//  digitalWrite(DigOutBlinkPin2, !blinkState);

  digitalWrite(DigOutBlinkPin1, LOW);
  digitalWrite(DigOutBlinkPin2, HIGH);

}

// Update timer status'
void processTimerEvent() {
  unsigned int timerStatePrev  = timerState;
  unsigned int timerStatePrev2 = timerState2;
  
// Turn the first relay OFF if the timer has run down
  if (timerState == 1){
    timerFullTime--;
    displayTimeout = now;
    if (timerFullTime <= 0){
      timerState = 0;
      processDigOutEvent();
      timerStopTime = now;
      Serial_print_s("Stopping Timer (Timer Completed). \r\n");
      Serial_print_s("timerFullTime: ");
      Serial_print_i(timerFullTime);
      Serial_print_s(" \r\n ");
      beepMid();
    }
    else if (timerFullTime < 5){    
      beepMid();                                        // Beep for the last 5 seconds
    }
  }

// Turn the second relay ON or OFF one second after the first relay
  if (timerState == 1){
    if (now - timerStartTime > 1000000){
      timerState2 = 1;
    }
  }
  else{
    if (now - timerStopTime > 1000000){
      timerState2 = 0;
    }
  }

  if ( (timerState == 0) && (timerState2 == 0) && (timerStatePrev2 == 1) && (timerFullTime <= 0) ){
    processDisplayUpdateEvent();
    timerFullTime = timerFullTimeBck;

    // Update the display
    displayReset();
  //displayUpdateDigit(0, 12 );
    displayUpdateDigit(1, 10 );
    displayUpdateDigit(2, 11 );
    displayUpdateDigit(3, 11 );

    beepLong();                                       // Beep because we are done
    delay(2000);
  }
}

void setup() {  
  // Initialize the Serial port
  Serial_begin(9600);
  delay(50);

  pinMode(DigInPin1, INPUT_PULLUP);
  pinMode(DigInPin2, INPUT_PULLUP);
  pinMode(DigInPin3, INPUT_PULLUP);

  pinMode(DigOutBlinkPin1, OUTPUT);
  pinMode(DigOutBlinkPin2, OUTPUT);

  pinMode(DigOutBuzzerPin, OUTPUT);

  pinMode(DigOutHeaterRelayPin1, OUTPUT);
  pinMode(DigOutHeaterRelayPin2, OUTPUT);

  pinMode(strobe, OUTPUT);
  pinMode(clock, OUTPUT);
  pinMode(data, OUTPUT);

  // Get the start time
  now = micros();
//  start            = now;
  lastDigInHop     = now;
  lastDigOutHop    = now;
  lastSerialOutHop = now;
  lastTimerHop     = now;
  lastBlinkHop     = now;
  displayTimeout   = now;
  displayOn();
  beepMid();
}

void loop() {  
  if (now - lastBlinkHop >= durationBlink){
    lastBlinkHop = now;
    processBlinkEvent();
  }
  if (now - lastTimerHop >= durationTimer){
    lastTimerHop = now;
    processTimerEvent();
  }
  if (now - lastDisplayHop >= durationDisplay){
    lastDisplayHop = now;
    processDisplayUpdateEvent();
  }
  if (now - lastSerialOutHop >= durationSerialOut){
    lastSerialOutHop = now;
    processSerialOutEvent();
  }
  if (now - lastDigInHop >= durationDigIn){
    lastDigInHop = now;
    processDigInEvent();
  }
  if (now - lastDigOutHop >= durationDigOut){
    lastDigOutHop = now;
    processDigOutEvent();
  }

  now = micros();
}
