# 1 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\dgp_snooze_1813\\dgp_snooze_1813.ino"
# 2 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\dgp_snooze_1813\\dgp_snooze_1813.ino" 2

SnoozeDigital digital;
SnoozeBlock config(digital);

void setup() {
  // Establish serial.
  Serial.begin(115200);

  // Configure snooze to wake on pin 15. 
  // Center button of preamp board.
  Serial.println("Setting up snoozeDigital block");
  digital.pinMode(15, 2, 3);
}

void loop() {
  // Go to sleep now.
  Serial.println("Going to sleep");
  Snooze.sleep( config );

  // We just woke up.
  // Re-establish serial.
  Serial.begin(115200);
  delay(100);
  Serial.println("Waking up");
  delay(1000);
  Serial.println("Still Awake");
  delay(1000);
}
