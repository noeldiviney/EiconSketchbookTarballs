# 1 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        // ***************************************************
        //-------------------------------------
        // Define one of these only
        //-------------------------------------

        //#define LED_ONLY          // Go through full setup, then just blink the LED
        //#define LED_AND_OLED        // Go through full setup, keep up the OLED and blink the LED

        //-------------------------------------
        // Vars used for pulsing the Blue LED
        //-------------------------------------
# 27 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        // ***************************************************
# 36 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        // #define USE_TLV320 0 - removed, TLV320AIC3254 'leaks' x-talk from inputs to outputs.




        //just a way to use the OPTIONS value to include the new sample rate.




        // we lock up, see how we go.
        //#define LOCKITUP

        //#define FOR_ANMI
        //#define SINEINOUTPUT
        //#define AUDIO_INTERFACE - don't define this, use the 'tools->USB Type' menu above to try this.

        //#define OPTIONS (USE_ADAU1372+USE_MICROPHONE+USE_48000)
# 94 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        /* libraries */


# 98 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 99 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 100 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 101 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 102 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2


        //#warning Using OLED libraries
# 106 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 107 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2


# 110 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 111 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2


# 114 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
        //#include <gTuner2.h>


# 118 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 119 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 120 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 2
# 128 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        const char labelStart[]="Eicon (VIC)";
# 140 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        /*
        #define VALUE_TO_STRING(x) #x
        #define VALUE(x) VALUE_TO_STRING(x)
        #define VAR_NAME_VALUE(var) #var "="  VALUE(var)

        #pragma message(VAR_NAME_VALUE(F_BUS))
        */


        /*** Pinning ***/

        /* Pins lost to I2S */
# 162 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        /* Pins lost to I2C */



        // vbsens is MCU 9 == A10



        // Codec shutdown on MCU 55 == 29






        // Jacked.L is MCU 53 == 28


        // self-latch-on  MCU 52 == 12




        // tuning circuit 'slapper-enable' is MCU 27 == 24


        // slapper pin is MCU 51 == 11


        // OLED reset on MCU 54 == 27

        // OLED SA0 on MCU 62 == 20






        // unit off after 30 seconds no activity when in tuner mode only.
# 211 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        // Tuner - perhaps switch these to inside the control block so config can be changed.

        // Changed by Simbop 29-04-2019
        //  #define TUNERAGCMINTHRESH 0.0009


        //  #define TUNERAGCGAINMAX 0.75
# 243 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        /*** Calc ***/
# 259 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        // Changed by Simon to make the volume adjustments more aggressive  
        //  #define ENCVALUESPERCLICK 4


        // Changed by Simon to make the volume adjustments more aggressive  
        //  #define ENCODER_PACE 50


        // Changed by Simon to make the volume adjustments more aggressive  
        //  #define ENCODER_DIVISOR 400


        /* EN1{57=2,58=14,59=7}; */



        /* EN2{60=8,61=6,43=15}; */



        /* EN3{63=21,64=5,1=31}; */
# 307 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        typedef struct {
            uint8_t type=7;
            float fC=1000;
            float Q=0.7071;
            float dB=0;
        } filter_t; // 13 bytes
# 325 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        typedef struct {
            float fC=1000;
            float Q=0.7071;
        } statvar_t; // 8 bytes(?)

        typedef struct {
            float volume=0.5;
            float b2f=0;
            float bf2m=0;
            float bass=0.5;
            float mids=0.5;
            float treb=0.5;
        } levels_t;

        typedef struct {
            float volume;
            float bass;
            float mids;
            float treb;
        } level_targets_t;

        typedef struct {
            uint8_t token;
            uint32_t seryr; // Serialisation data, the date
            uint32_t serhr; // Serialisation data, the time
        } ser_data_t;


        typedef struct {


        // Values of 0 to 1 controlled by rotary encoders.
        levels_t levs;


        // Level limits
        float bridgeLevel[2]; // 0
        float faceLevel[2]; // 1 
        float brfaLevel[2]; // 2
        float micLevel[2]; // 3
        float outputLevel[2]; // eg. vols[0]=-60dB, vols[1]=0dB - use fmap to map volume between [0]<->[1]

        float bassLevel[2]; // 5
        float midsLevel[2]; // 6
        float trebLevel[2]; // 7

        float rampRate;
        float virtualCenter;

        // filters
        statvar_t svFilters[3];
        filter_t bridgeFilters[5];



        filter_t faceFilters[5];



        filter_t micFilters[5];



        filter_t brfaFilters[5];



        filter_t finalFilters[4];




        // Codec Control
        uint8_t adc_enable; // bitwise enable or disable of the inputs 0,1,2 - don't care about number 3 here.
        uint8_t adc_levels[3];
        uint8_t dac_level;
        uint8_t pga_enable; // bitwise like adc_enable
        uint8_t pga_levels[3];
        uint8_t slew_enable; // bitwise...
        uint8_t slew_rate; // one singular setting for all

        float battery_low[2];
        float battery_dead[2];
        float battery_switch;

        float battery_max[2]; // to display battery scale from
        // Tuner Control
        float tune_scope;
        uint8_t minSamplePairs;
        uint8_t minMatchedSamples;

        //  statvar_t tuningFilters[2];        // bounds warning
        statvar_t tuningFilters[4]; // bounds warning

        // special strings
        //  uint8_t face_label[9];        // bounds warning
        //  uint8_t mic_label[9];        // bounds warning
        uint8_t face_label[10]; // bounds warning
        uint8_t mic_label[10]; // bounds warning



} master_t; // lots of bytes...

/*****************************************************************************************************************/

/**** Audio pass-thru to help keep streams in phase ****/
class AudioPass : public AudioStream
{
        public:
            AudioPass(void) : AudioStream(1, inputQueueArray) { }
            bool acted(void);
            virtual void update(void);
        private:
            audio_block_t *inputQueueArray[1];
            uint8_t n_acted=0;
}; //  class AudioPass : public AudioStream

bool AudioPass::acted(void)
{
        if(n_acted)
        {
            n_acted=0;
            return true;
        }
        return false;
}//  bool AudioPass::acted(void)


void AudioPass::update(void)
{
        n_acted|=1;
        audio_block_t *out = 
# 457 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3 4
                            __null
# 457 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                ;
        out = receiveWritable();
        if (out)
        {
            transmit(out);
            release(out);
        }
}//  void AudioPass::update(void)
/*******************************************************/



        // these would probably be better as enum but not yet.
# 484 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
void calcBQ2(unsigned char filtertype, double Fc, double Q, float *coefs, double peakGain, double Fs, double quantize) {
        double norm;
        quantize=1; // callers always use the regular biquad quantization value, this float (or double) based set doesn't need that.

        double V = pow(10, fabs(peakGain) / 20);
        double K = tan(((
# 489 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                      3.14159265358979323846
# 489 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                      /1000)*895) * Fc / Fs);
        switch (filtertype)
        {
            /*case "one-pole lp":
                coefs[3] = Math.exp(-2.0 * Math.PI * (Fc / Fs));
                coefs[0] = 1.0 - coefs[3];
                coefs[3] = -coefs[3];
                coefs[1] = coefs[2] = coefs[4] = 0;
                break;
            
            case "one-pole hp":
                coefs[3] = -Math.exp(-2.0 * Math.PI * (0.5 - Fc / Fs));
                coefs[0] = 1.0 + coefs[3];
                coefs[3] = -coefs[3];
                coefs[1] = coefs[2] = coefs[4] = 0;
                break;
                                  */
            case 0:
                norm = quantize / (1 + K / Q + K * K);
                coefs[0] = K * K * norm;
                coefs[1] = 2 * coefs[0];
                coefs[2] = coefs[0];
                coefs[3] = 2 * (K * K - 1) * norm;
                coefs[4] = (1 - K / Q + K * K) * norm;
                break;

            case 1:
                norm = quantize / (1 + K / Q + K * K);
                coefs[0] = 1 * norm;
                coefs[1] = -2 * coefs[0];
                coefs[2] = coefs[0];
                coefs[3] = 2 * (K * K - 1) * norm;
                coefs[4] = (1 - K / Q + K * K) * norm;
                break;

            case 2:
                norm = quantize / (1 + K / Q + K * K);
                coefs[0] = K / Q * norm;
                coefs[1] = 0;
                coefs[2] = -coefs[0];
                coefs[3] = 2 * (K * K - 1) * norm;
                coefs[4] = (1 - K / Q + K * K) * norm;
                break;

            case 3:
                norm = quantize / (1 + K / Q + K * K);
                coefs[0] = (1 + K * K) * norm;
                coefs[1] = 2 * (K * K - 1) * norm;
                coefs[2] = coefs[0];
                coefs[3] = coefs[1];
                coefs[4] = (1 - K / Q + K * K) * norm;
                break;

            case 4:
                if (peakGain >= 0)
                {
                    norm = quantize / (1 + 1/Q * K + K * K);
                    coefs[0] = (1 + V/Q * K + K * K) * norm;
                    coefs[1] = 2 * (K * K - 1) * norm;
                    coefs[2] = (1 - V/Q * K + K * K) * norm;
                    coefs[3] = coefs[1];
                    coefs[4] = (1 - 1/Q * K + K * K) * norm;
                }
                else
                {
                    norm = quantize / (1 + V/Q * K + K * K);
                    coefs[0] = (1 + 1/Q * K + K * K) * norm;
                    coefs[1] = 2 * (K * K - 1) * norm;
                    coefs[2] = (1 - 1/Q * K + K * K) * norm;
                    coefs[3] = coefs[1];
                    coefs[4] = (1 - V/Q * K + K * K) * norm;
                }
                break;
            case 5:
                if (peakGain >= 0)
                {
                    norm = quantize / (1 + 
# 565 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                          1.41421356237309504880 
# 565 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                                  * K + K * K);
                    coefs[0] = (1 + sqrt(2*V) * K + V * K * K) * norm;
                    coefs[1] = 2 * (V * K * K - 1) * norm;
                    coefs[2] = (1 - sqrt(2*V) * K + V * K * K) * norm;
                    coefs[3] = 2 * (K * K - 1) * norm;
                    coefs[4] = (1 - 
# 570 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                   1.41421356237309504880 
# 570 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                           * K + K * K) * norm;
                }
                else
                {
                    norm = quantize / (1 + sqrt(2*V) * K + V * K * K);
                    coefs[0] = (1 + 
# 575 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                   1.41421356237309504880 
# 575 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                           * K + K * K) * norm;
                    coefs[1] = 2 * (K * K - 1) * norm;
                    coefs[2] = (1 - 
# 577 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                   1.41421356237309504880 
# 577 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                           * K + K * K) * norm;
                    coefs[3] = 2 * (V * K * K - 1) * norm;
                    coefs[4] = (1 - sqrt(2*V) * K + V * K * K) * norm;
                }
                break;
            case 6:
                if (peakGain >= 0)
                {
                    norm = quantize / (1 + 
# 585 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                          1.41421356237309504880 
# 585 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                                  * K + K * K);
                    coefs[0] = (V + sqrt(2*V) * K + K * K) * norm;
                    coefs[1] = 2 * (K * K - V) * norm;
                    coefs[2] = (V - sqrt(2*V) * K + K * K) * norm;
                    coefs[3] = 2 * (K * K - 1) * norm;
                    coefs[4] = (1 - 
# 590 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                   1.41421356237309504880 
# 590 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                           * K + K * K) * norm;
                }
                else
                {
                    norm = quantize / (V + sqrt(2*V) * K + K * K);
                    coefs[0] = (1 + 
# 595 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                   1.41421356237309504880 
# 595 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                           * K + K * K) * norm;
                    coefs[1] = 2 * (K * K - 1) * norm;
                    coefs[2] = (1 - 
# 597 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3
                                   1.41421356237309504880 
# 597 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                           * K + K * K) * norm;
                    coefs[3] = 2 * (K * K - V) * norm;
                    coefs[4] = (V - sqrt(2*V) * K + K * K) * norm;
                }
                break;
            case 7:
                coefs[0]=quantize;
                coefs[1]=0;
                coefs[2]=0;
                coefs[3]=0;
                coefs[4]=0;
        }

}
# 744 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
        if(out_max>out_min)
        {
            return (x-in_min)*(out_max-out_min)/(in_max-in_min)+out_min;
        }
        else
        {
            return -((x-in_max)*(out_max-out_min))/(in_min-in_max)+out_max;
        }
}//  float fmap()

filter_t mFilter(float portion, filter_t flt1, filter_t flt2)
{
        filter_t temp;
        temp.type=flt1.type;
        temp.fC=fmap(portion,0,1,flt1.fC,flt2.fC);
        temp.Q=fmap(portion,0,1,flt1.Q,flt2.Q);
        temp.dB=fmap(portion,0,1,flt1.dB,flt2.dB);
        return temp;
}//  filter_t mFilter()


        ADC *adc = new ADC(); // adc object
        uint16_t adc_max;

        /*****************************************************************************************************************/

        /*** Audio Object Instantiation ***/

        AudioInputI2SQuadslave i2s_In;

        // tier 1 objects
        AudioFilterBiquad biquad_bridge;
        AudioFilterBiquad biquad_face;
        AudioPass mic_pass1;


        // interlaced objects (For tuning purposes)
        AudioPass tune_pass1;
        AudioAnalyzePeak peak_bridge;



        // tier 2 objects
        AudioMixer4 mixer_bf;
        AudioPass mic_pass2;

        AudioPass tune_pass2;


        // tier 3 objects
        AudioFilterBiquad biquad_brfa;
        AudioFilterBiquad biquad_mic;

        AudioPass tune_pass3;


        // tier 4 object
        AudioMixer4 mixer_bfm;
        // interlaced
        AudioAnalyzePeak peakBfm;

        // Tier 5

        AudioFilterStateVariable filterBass; // svfilt[1] and svfilt[3]
        AudioPass bass_pass;
        AudioFilterStateVariable filterMidTreb; // svfilt[2] and svfilt[4]

        AudioMixer4 mixer_tone;

        // tier 6 object
        AudioFilterBiquad biquad_final;

        // interlaced
        AudioAnalyzePeak peakOut;

        // tier 6/7 ?
        AudioFilterStateVariable filter1; // highpass - consider svfilt[0]





        // tier 7 object
        AudioMixer4 mixer_final; // volume control.






        AudioOutputI2SQuadslave i2s_Out;


        /*** Audio Pathway Threading ***/

        // tier 1

        AudioConnection pc_bridgeIn(i2s_In,0,biquad_bridge,0);
        AudioConnection pc_faceIn(i2s_In,1,biquad_face,0);

        AudioConnection pc_micIn(i2s_In,2,mic_pass1,0);




        AudioConnection pc_Tn0(i2s_In,0,peak_bridge,0); // for Tuner AGC
        AudioConnection pc_Tn1(i2s_In,0,tune_pass1,0); // 


        // tier 2
        AudioConnection pc_bf1(biquad_bridge,0,mixer_bf,0);
        AudioConnection pc_bf2(biquad_face,0,mixer_bf,1);
        AudioConnection pc_mp1(mic_pass1,0,mic_pass2,0);

        AudioConnection pcTn2(tune_pass1,0,tune_pass2,0);


        // tier 3
        AudioConnection pc_bff(mixer_bf,0,biquad_brfa,0);
        AudioConnection pc_mflt(mic_pass2,0,biquad_mic,0);

        AudioConnection pcTn3(tune_pass2,0,tune_pass3,0);



        // tier 4
        AudioConnection pc_bfm(biquad_brfa,0,mixer_bfm,0);
        AudioConnection pc_mbf(biquad_mic,0,mixer_bfm,1);

        AudioConnection pcTn4(tune_pass3,0,mixer_bfm,2);


        // tier 5
        // AudioConnection               pc_sff(mixer_bfm,0,biquad_semi,0);
        AudioConnection pc_sff1(mixer_bfm,0,filterBass,0);

        // interlaced
        AudioConnection pc_pkBfm(mixer_bfm,0,peakBfm,0);


        // change to tone controls use state variable filter
        AudioConnection pc_sff2(filterBass,0,bass_pass,0);
        AudioConnection pc_sff3(filterBass,2,filterMidTreb,0);

        AudioConnection pc_sff4(bass_pass,0,mixer_tone,0);
        AudioConnection pc_sff5(filterMidTreb,0,mixer_tone,1);
        AudioConnection pc_sff6(filterMidTreb,2,mixer_tone,2);





        AudioConnection pc_sff7(mixer_tone,0,biquad_final,0);


        // tier 7
        AudioConnection pc_fvm(biquad_final,0,mixer_final,0);





        AudioConnection pc_flt(mixer_final,0,filter1,0);

        // interlaced
        AudioConnection pc_pkOut(filter1,2,peakOut,0);
        //AudioConnection               pc_pkOut(biquad_final,0,peakOut,0);
        // out
        AudioConnection pc_out(filter1,2,i2s_Out,0);
# 923 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        AudioConnection pc_tun(mixer_tone,0,i2s_Out,1);


        /****************************************************************************************************************/

        AudioControlADAU1372 adau1372;

        typedef enum {isoffactually,preamp,tuner} mainMode_t;
        uint8_t mainMode=0,profileMode,runLevel=0,maxProfiles=2; // shared - tuner mode is just muting when OPTIONS&USE_OLED==0

        float batteryReading=9;


        Adafruit_SSD1306 display(27);
        gTuner myTuner;
        __attribute__ ((section(".fastrun"), noinline, noclone )) void slapper_isr(void)
        {
            myTuner.inSample();
        }

        typedef enum {dispVols,dispTone,dispLogo,dispLogo2,dispProfile,dispBatt,dispTuner} dispMode_t;
        typedef enum {volctrls,tonectrls} ctrlMode_t;

        elapsedMillis display_timeout,encoder_pace;

        uint8_t ctrlMode=0,dispflags=0; // 128;
        /*  runLevel
         *  7 Starting up, cleared by logo being cleared from display
         *  6 -
         *  5 -
         *  4 -
         *  3 -
         *  2 -
         *  1 Tuning mode operational
         *  0 Jacked In
         */

        /* dispflags
         * 7 - Display UP
         * 6 - Self Latched.
         * 5 -
         * 4 -
         * 3:0 - display mode
         */

        uint16_t tuner_lastr=65535,tuner_read=0;
        float tuner_lastDiff,tuner_lastDetected;

        //                               0 1 234 56 7 890 12 3 4 56789012345678901234567890123456789012345678901
        const unsigned char notenames[]="C\0\0C#\0D\0\0D#\0E\0\0F\0\0F#\0G\0\0G#\0A\0\0A#\0B\0\0_\0\0_\0\0_\0\0";
        const unsigned char notelens[]={1,2,1,2,1,1,2,1,2,1,2,1,1};

        /* Logo arrays */
        uint8_t logo_data[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,3,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,7,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,
            15,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,15,240,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,192,0,0,0,0,0,0,0,0,28,0,0,0,0,112,31,128,0,0,0,0,0,0,0,0,28,0,0,0,0,112,
            31,128,0,224,255,135,252,31,7,0,61,224,31,131,254,120,31,0,0,225,255,143,254,31,7,0,56,240,31,135,254,56,31,255,252,227,224,31,31,31,135,0,120,240,63,143,128,60,31,255,252,231,128,62,7,159,199,0,120,248,63,158,0,60,
            31,255,252,231,128,60,7,159,199,0,120,120,123,158,0,28,31,255,252,231,128,60,7,159,231,0,112,120,123,158,0,28,31,0,0,231,0,56,3,157,231,0,112,60,123,156,0,28,31,128,0,231,0,56,3,156,247,0,112,60,243,156,0,28,
            31,128,0,231,0,56,3,156,247,0,112,30,243,156,0,28,31,192,0,231,0,60,7,156,127,0,112,31,227,156,0,60,15,240,0,231,128,60,7,156,127,0,120,31,227,158,0,60,15,255,252,231,128,60,7,156,63,0,120,15,227,158,0,60,
            7,255,252,227,224,31,31,28,63,0,56,15,195,143,128,56,3,255,252,225,255,143,254,28,31,0,60,7,195,135,254,120,0,255,252,224,255,135,252,28,15,0,28,7,131,131,254,112,0,0,0,0,0,0,0,0,0,0,28,0,0,0,0,112,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,128,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,136,196,98,32,113,28,56,176,34,142,1,195,139,48,0,0,136,196,98,32,137,34,68,200,34,145,2,36,76,200,0,
            0,85,42,149,64,137,32,68,136,20,144,2,4,72,136,0,0,85,42,149,64,249,32,68,139,148,144,2,4,72,136,0,0,85,42,149,64,129,32,68,136,20,144,2,4,72,136,0,0,34,17,8,128,137,34,68,136,8,145,2,36,72,136,0,
            0,34,17,8,136,113,28,56,136,8,142,33,195,136,136,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        uint8_t logo2_data[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,
            3,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,7,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,0,0,0,0,0,0,15,240,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            31,192,0,0,0,0,0,0,0,0,28,0,0,0,0,112,31,128,0,0,0,0,0,0,0,0,28,0,0,0,0,112,31,128,0,224,255,135,252,31,7,0,61,224,31,131,254,120,31,0,0,225,255,143,254,31,7,0,56,240,31,135,254,56,
            31,255,252,227,224,31,31,31,135,0,120,240,63,143,128,60,31,255,252,231,128,62,7,159,199,0,120,248,63,158,0,60,31,255,252,231,128,60,7,159,199,0,120,120,123,158,0,28,31,255,252,231,128,60,7,159,231,0,112,120,123,158,0,28,
            31,0,0,231,0,56,3,157,231,0,112,60,123,156,0,28,31,128,0,231,0,56,3,156,247,0,112,60,243,156,0,28,31,128,0,231,0,56,3,156,247,0,112,30,243,156,0,28,31,192,0,231,0,60,7,156,127,0,112,31,227,156,0,60,
            15,240,0,231,128,60,7,156,127,0,120,31,227,158,0,60,15,255,252,231,128,60,7,156,63,0,120,15,227,158,0,60,7,255,252,227,224,31,31,28,63,0,56,15,195,143,128,56,3,255,252,225,255,143,254,28,31,0,60,7,195,135,254,120,
            0,255,252,224,255,135,252,28,15,0,28,7,131,131,254,112,0,0,0,0,0,0,0,0,0,0,28,0,0,0,0,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,128,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,136,196,98,32,113,28,56,176,34,142,1,195,139,48,0,0,136,196,98,32,137,34,68,200,34,145,2,36,76,200,0,
            0,85,42,149,64,137,32,68,136,20,144,2,4,72,136,0,0,85,42,149,64,249,32,68,139,148,144,2,4,72,136,0,0,85,42,149,64,129,32,68,136,20,144,2,4,72,136,0,0,34,17,8,128,137,34,68,136,8,145,2,36,72,136,0,
            0,34,17,8,136,113,28,56,136,8,142,33,195,136,136,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
# 1019 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        Encoder encL(21, 5);
        butts butSelect=butts(31,25);
        Encoder encM(8, 6);
        butts butMode=butts(7,25);
        Encoder encR(2, 14);

        butts butMiddle=butts(15,25);

        butts butJack=butts(28,25);
# 1041 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        LineReceiver rcvr(127,consoleParser);

        ser_data_t mSer; // Serialisation data here...
        master_t mCon; // all the parameters here...

        /* #ifdef HAS_ENCODERS
           #define master_volume mCon.levs.volume
           #define master_b2f    mCon.levs.b2f
           #define master_bf2m   mCon.levs.bf2m
           #define master_bass   mCon.levs.bass
           #define master_mids   mCon.levs.mids
           #define master_treb   mCon.levs.treb
           #else */
        levels_t levs;






    /* #endif */

        level_targets_t actlevs[2]; // one set is what we are, the other set is what we want.
        /* actual_levels.volume=0;
           actual_levels.brfa=0;
           actual_levels.bf2m=0;
           actual_levels.bass=0;
           actual_levels.mids=0;
           actual_levels.treb=0; 
		 */


        const uint8_t tie_downs[]={A11,A12,A13,33,3,4,16,17,1,32,25,10






        ,0

        ,255
        };
        // ending on 255 to make it obvious because 0 is a pin!




void applyFilter(uint8_t bqNum, uint8_t setNum)
{



        float cco[5];

        filter_t tFilt;
        setNum&=3; // no more than 0 to 3.
        switch(bqNum)
        {
            case 0: // bridgeFilters[5];
                if(setNum>0)
                {


                    calcBQ2(mCon.bridgeFilters[setNum].type&7,mCon.bridgeFilters[setNum].fC,mCon.bridgeFilters[setNum].Q,cco,mCon.bridgeFilters[setNum].dB,48000,1073741824);
                    biquad_bridge.jetCoefficients(setNum,cco);




                }
                else
                {
                    // filter_t mFilter(float portion, filter_t flt1, filter_t flt2)
                    tFilt=mFilter(levs.b2f,mCon.bridgeFilters[setNum],mCon.bridgeFilters[setNum+4]);
                    calcBQ2(tFilt.type,tFilt.fC,tFilt.Q,cco,tFilt.dB,48000,1073741824);
                    biquad_bridge.jetCoefficients(setNum,cco);
                }
                break;
            case 1: // faceFilters[5];
                if(setNum>0)
                {

                    calcBQ2(mCon.faceFilters[setNum].type&7,mCon.faceFilters[setNum].fC,mCon.faceFilters[setNum].Q,cco,mCon.faceFilters[setNum].dB,48000,1073741824);
                    biquad_face.jetCoefficients(setNum,cco);




                }
                else
                {
                    tFilt=mFilter(levs.b2f,mCon.faceFilters[setNum],mCon.faceFilters[setNum+4]);
                    calcBQ2(tFilt.type,tFilt.fC,tFilt.Q,cco,tFilt.dB,48000,1073741824);
                    biquad_face.jetCoefficients(setNum,cco);
                }
                break;
            case 2: // brfaFilters[5];
                if(setNum>0)
                {

                    calcBQ2(mCon.brfaFilters[setNum].type&7,mCon.brfaFilters[setNum].fC,mCon.brfaFilters[setNum].Q,cco,mCon.brfaFilters[setNum].dB,48000,1073741824);
                    biquad_brfa.jetCoefficients(setNum,cco);




                }
                else
                {
                    tFilt=mFilter(levs.bf2m,mCon.brfaFilters[setNum],mCon.brfaFilters[setNum+4]);
                    calcBQ2(tFilt.type,tFilt.fC,tFilt.Q,cco,tFilt.dB,48000,1073741824);
                    biquad_brfa.jetCoefficients(setNum,cco);
                }
                break;
            case 3: // micFilters[5];
                if(setNum>0)
                {

                    calcBQ2(mCon.micFilters[setNum].type&7,mCon.micFilters[setNum].fC,mCon.micFilters[setNum].Q,cco,mCon.micFilters[setNum].dB,48000,1073741824);
                    biquad_mic.jetCoefficients(setNum,cco);




                }
                else
                {
                    tFilt=mFilter(levs.bf2m,mCon.micFilters[setNum],mCon.micFilters[setNum+4]);
                    calcBQ2(tFilt.type,tFilt.fC,tFilt.Q,cco,tFilt.dB,48000,1073741824);
                    biquad_mic.jetCoefficients(setNum,cco);
                }
                break;
            //  case 5: // finalFilters[4];
            default: // always going to screw over the final filters if a wrong bqNum is used.

                calcBQ2(mCon.finalFilters[setNum].type&7,mCon.finalFilters[setNum].fC,mCon.finalFilters[setNum].Q,cco,mCon.finalFilters[setNum].dB,48000,1073741824);
                biquad_final.jetCoefficients(setNum,cco);




        }
}


void applyVolume(void)
{
        float nn=fmap(0.5,0,1,mCon.outputLevel[0],mCon.outputLevel[1]);
        // Changed by Simon to address laggy volume encoder responce 
        // Removed rush to ZERO
        /*
            if(master_volume==0)
            {
               // mixer_final.gain(0,0); // absolutely silence the output
               actlevs[1].volume=dB2ratio(-120);
            } 
		    else 
		    {
               if(master_volume<=mCon.virtualCenter)
                {
                    // mixer_final.gain(0,dB2ratio(fmap(mCon.volume,0,0.25,mCon.outputLevel[0],nn)));
                    actlevs[1].volume=dB2ratio(fmap(master_volume,0,mCon.virtualCenter,mCon.outputLevel[0],nn));
                } 
			    else 
			    {
                    // mixer_final.gain(0,dB2ratio(fmap(mCon.volume,0.25,1,nn,mCon.outputLevel[1])));
                    actlevs[1].volume=dB2ratio(fmap(master_volume,mCon.virtualCenter,1,nn,mCon.outputLevel[1]));
                }
            }
         */

    // Removed different behavure above and bellow half volume

        if(levs.volume<=mCon.virtualCenter)
        {
            // mixer_final.gain(0,dB2ratio(fmap(mCon.volume,0,0.25,mCon.outputLevel[0],nn)));
            actlevs[1].volume=exp(fmap(levs.volume,0,mCon.virtualCenter,mCon.outputLevel[0],nn)/10);
        }
        else
        {
            // mixer_final.gain(0,dB2ratio(fmap(mCon.volume,0.25,1,nn,mCon.outputLevel[1])));
            actlevs[1].volume=exp(fmap(levs.volume,mCon.virtualCenter,1,nn,mCon.outputLevel[1])/10);
        }//  if(master_volume<=mCon.virtualCenter)


    //  actlevs[1].volume=dB2ratio(fmap(master_volume,0,mCon.virtualCenter,mCon.outputLevel[0],nn));

}


        void applyB2f(void)
        {
            mixer_bf.gain(0,exp(fmap(levs.b2f,0,1,mCon.bridgeLevel[0],mCon.bridgeLevel[1])/10));
            applyFilter(0,0); // bridge_biquad set 0
            mixer_bf.gain(1,-exp(fmap(levs.b2f,0,1,mCon.faceLevel[0],mCon.faceLevel[1])/10)); /* ******************** FACE INVERTED HERE ************************** */
            applyFilter(1,0); // face biquad set 0
        }

        void applyBf2m(void)
        {
            mixer_bfm.gain(0,exp(fmap(levs.bf2m,0,1,mCon.brfaLevel[0],mCon.brfaLevel[1])/10));
            applyFilter(3,0); // brfa_biquad set 0
            mixer_bfm.gain(1,exp(fmap(levs.bf2m,0,1,mCon.micLevel[0],mCon.micLevel[1])/10));
           applyFilter(2,0); // mic_biquad set 0
        }

        void applyBass(void)
        {
            // applyFilter(4,0);
            // mixer_tone.gain(0,mCon.bass);
            actlevs[1].bass=exp(fmap(levs.bass,0,1,mCon.bassLevel[0],mCon.bassLevel[1])/10);
        }

        void applyMids(void)
        {
            // applyFilter(4,1);
            // mixer_tone.gain(1,-mCon.mids);
            actlevs[1].mids=-exp(fmap(levs.mids,0,1,mCon.midsLevel[0],mCon.midsLevel[1])/10);
        }

        void applyTreb(void)
        {
            // applyFilter(4,2);
            // mixer_tone.gain(2,mCon.treb);
            actlevs[1].treb=exp(fmap(levs.treb,0,1,mCon.trebLevel[0],mCon.trebLevel[1])/10);
        }

        ser_data_t getSerialData(void)
        {
            ser_data_t temporary;
            unsigned int eeA=0xFFF -(sizeof(temporary)+2);
            EEPROM.get(eeA,temporary);
            if(temporary.token!=(3+1 +2 +2 +(1|2|4|8|16|32|64)))
            {
                temporary.token=(3+1 +2 +2 +(1|2|4|8|16|32|64));
                temporary.seryr=0;
                temporary.serhr=0;
            }
            return temporary;
        }

        void setSerialData(ser_data_t temporary)
        {
            unsigned int eeA=0xFFF -(sizeof(temporary)+2);
            EEPROM.put(eeA,temporary); // for better or worse, this is now stored (reasonably) permanently.
        }

        unsigned char returnLastProfile()
        {
            unsigned int eeA=0xFFF;
            uint8_t nnn=0;
            EEPROM.get(eeA,nnn);
            eeA=0xFFF -1; // preset it to where we expect to read from...
            if(nnn==(3+1 +2 +2 +(1|2|4|8|16|32|64)))
            {
                return EEPROM.read(eeA);
            }
            else
            {
                return 0;
            }
        }

        unsigned char saveLastProfile()
        {
            unsigned int eeA=0xFFF;
            uint8_t nnn=(3+1 +2 +2 +(1|2|4|8|16|32|64));
            if(EEPROM.read(eeA)!=nnn) EEPROM.put(eeA,nnn);
            eeA=0xFFF -1;
            if(EEPROM.read(eeA)!=profileMode) EEPROM.put(eeA,profileMode);
            return eeA;
        }

        unsigned char saveLevs()
        {
            unsigned int eeA=0xFFF -(sizeof(mSer)+sizeof(levs)+3);
            uint8_t nnn=(3+1 +2 +2 +(1|2|4|8|16|32|64));
            if(EEPROM.read(eeA)!=nnn) EEPROM.put(eeA,nnn);
            eeA++;
            EEPROM.put(eeA,levs);
            return eeA;
        }

        unsigned char getLevs()
        {
            unsigned int eeA=0xFFF -(sizeof(mSer)+sizeof(levs)+3);
            uint8_t nnn=(3+1 +2 +2 +(1|2|4|8|16|32|64));
            if(EEPROM.read(eeA)==nnn)
            {
                EEPROM.get(++eeA,levs);
            }
            else
            {
                // preset default levs - these macros target the right stuff
                levs.volume=0.5;
                levs.b2f=0;
                levs.bf2m=0;
                levs.bass=0.5;
                levs.mids=0.5;
                levs.treb=0.5;
            }
            applyVolume();
            applyB2f();
            applyBf2m();
            applyBass();
            applyMids();
            applyTreb();

            return eeA;
        }


        unsigned int getPromData()
        {
            // if(mainMode!=preamp) return 0;
            unsigned int eeA=profileMode*(sizeof(mCon)+2); // clearance of 1 byte between data blocks, yep, I wasted a byte.
            uint8_t nnn=0;
            // Serial.print("eeprom data is");
            EEPROM.get(eeA,nnn);
            eeA+=sizeof(nnn);
            if(nnn==(3+1 +2 +2 +(1|2|4|8|16|32|64)))
        {
            EEPROM.get(eeA,mCon);
            eeA+=sizeof(mCon);
            // Serial.println(" valid here.");
# 1375 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        }
        else
        {
            // Serial.println(" invalid here.");
            /*    master_volume=0.5;
                  master_b2f=0;
                  master_bf2m=0;
                  master_bass=0.5;
                  master_mids=0.5;
                  master_treb=0.5; 
             */

            mCon.bridgeLevel[0]=0;
            mCon.bridgeLevel[1]=0;
            mCon.faceLevel[0]=-20;
            mCon.faceLevel[1]=-5;
            mCon.brfaLevel[0]=0;
            mCon.brfaLevel[1]=0;

            mCon.micLevel[0]=-45;
            mCon.micLevel[1]=5;




            mCon.outputLevel[0]=-60;
            mCon.outputLevel[1]=5;

            mCon.bassLevel[0]=-23.2;
            mCon.bassLevel[1]=-3.2;
            mCon.midsLevel[0]=-27;
            mCon.midsLevel[1]=-7;
            mCon.trebLevel[0]=-20;
            mCon.trebLevel[1]=0;

            mCon.rampRate=exp(1/10); // 1 dB per full frame.
            mCon.virtualCenter=0.25; // a virtual center for the Volume control

            // Highpass
            mCon.svFilters[0].fC=21;
            mCon.svFilters[0].Q=0.7071;

            // Tone Controls
            mCon.svFilters[1].fC=170;
            mCon.svFilters[1].Q=0.7071;
            mCon.svFilters[2].fC=1700;
            mCon.svFilters[2].Q=0.7071;

            mCon.bridgeFilters[0].type=4;
            mCon.bridgeFilters[0].fC=14000;
            mCon.bridgeFilters[0].Q=0.24;
            mCon.bridgeFilters[0].dB=0;
# 1435 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
            mCon.bridgeFilters[1].type=4;

            mCon.bridgeFilters[1].fC=3000;
            mCon.bridgeFilters[1].Q=0.4;
            mCon.bridgeFilters[1].dB=0;

            if(profileMode==0)
            {
                mCon.bridgeFilters[2].type=7;
                mCon.bridgeFilters[3].type=7;
            }
            else
            {
# 1456 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                mCon.bridgeFilters[2].type=4;

                mCon.bridgeFilters[2].fC=370;
                mCon.bridgeFilters[2].Q=4;
                mCon.bridgeFilters[2].dB=-9;
# 1470 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                mCon.bridgeFilters[3].type=4;

                mCon.bridgeFilters[3].fC=247;
                mCon.bridgeFilters[3].Q=4;
                mCon.bridgeFilters[3].dB=-6;

            }

            mCon.bridgeFilters[4].type=4;
            mCon.bridgeFilters[4].fC=14000;
            mCon.bridgeFilters[4].Q=0.24;
            mCon.bridgeFilters[4].dB=-11;

            mCon.faceFilters[0].type=7;
# 1493 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
            mCon.faceFilters[1].type=1;

            mCon.faceFilters[1].fC=350;
            mCon.faceFilters[1].Q=0.7071;


            mCon.faceFilters[2].type=7;
            mCon.faceFilters[3].type=7;
            mCon.faceFilters[4].type=7;

            mCon.brfaFilters[0].type=4;

            mCon.brfaFilters[0].fC=20000;
            mCon.brfaFilters[0].Q=0.15;
            mCon.brfaFilters[0].dB=0;





            mCon.brfaFilters[1].type=7;
            mCon.brfaFilters[2].type=7;
            mCon.brfaFilters[3].type=7;
            mCon.brfaFilters[4].type=4;

            mCon.brfaFilters[4].fC=20000;
            mCon.brfaFilters[4].Q=0.15;
            mCon.brfaFilters[4].dB=-11;






            mCon.micFilters[0].type=7;
# 1536 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
            mCon.micFilters[1].type=1;

            mCon.micFilters[1].fC=2000;
            mCon.micFilters[1].Q=0.7071;

            mCon.micFilters[2].type=7;
            mCon.micFilters[3].type=7;
            mCon.micFilters[4].type=7;

            mCon.finalFilters[0].type=7; // FILTER_PARAEQ;
            mCon.finalFilters[0].fC=3400;
            mCon.finalFilters[0].Q=6;
            mCon.finalFilters[0].dB=-12;
            mCon.finalFilters[1].type=7;
            mCon.finalFilters[2].type=7;
            mCon.finalFilters[3].type=7;

            mCon.adc_enable=7|(1<<4); // 1<<4 refers micbias.
            mCon.adc_levels[0]=0;
            mCon.adc_levels[1]=0;
            mCon.adc_levels[2]=0;
            mCon.dac_level=0;
            mCon.pga_enable=7|(2<<4); // face has pga10+ boost
            mCon.pga_levels[0]=26;
            mCon.pga_levels[1]=25;
            mCon.pga_levels[2]=30;
            mCon.slew_enable=0;
            mCon.slew_rate=0;

            mCon.battery_low[0]=2.95;
            mCon.battery_low[1]=6.5;
            mCon.battery_dead[0]=2.825;
            mCon.battery_dead[1]=6;

            mCon.battery_switch=5.1;


           mCon.battery_max[0]=4.2;
           mCon.battery_max[1]=9;

           //mCon.tune_scope=0.1;
           mCon.tune_scope=0.12;
           //mCon.minSamplePairs=6;
           mCon.minSamplePairs=5;
           //mCon.minMatchedSamples=5;
           mCon.minMatchedSamples=4;

           mCon.tuningFilters[0].fC=33;
           mCon.tuningFilters[0].Q=0.7071;
           //mCon.tuningFilters[1].fC=880;
           mCon.tuningFilters[1].fC=600;
           mCon.tuningFilters[1].Q=0.7071;

           mCon.face_label[0]='F';
           mCon.face_label[1]='a';
           mCon.face_label[2]='c';
           mCon.face_label[3]='e';
           mCon.face_label[4]=0;
           mCon.face_label[5]=0;
           mCon.face_label[6]=0;
           mCon.face_label[7]=0;
           mCon.face_label[8]=0;
           mCon.face_label[9]=0;

           mCon.mic_label[0]='M';
           mCon.mic_label[1]='i';
           mCon.mic_label[2]='c';
           mCon.mic_label[3]=0;
           mCon.mic_label[4]=0;
           mCon.mic_label[5]=0;
           mCon.mic_label[6]=0;
           mCon.mic_label[7]=0;
           mCon.mic_label[8]=0;
           mCon.mic_label[9]=0;

        }

        // All the settings that were just read, or preset, need to be applied asap. (As Sensibly As Possible....)

        for(uint8_t i=0;i<4;i++)
        {
            applyFilter(0,i);
            applyFilter(1,i);
            applyFilter(2,i);
            applyFilter(3,i);
            applyFilter(4,i);
        }

        filter1.frequency(mCon.svFilters[0].fC);
        filter1.resonance(mCon.svFilters[0].Q);

        if(mainMode==preamp)
        {

            filterBass.frequency(mCon.svFilters[1].fC);
            filterBass.resonance(mCon.svFilters[1].Q);
            filterMidTreb.frequency(mCon.svFilters[2].fC);
            filterMidTreb.resonance(mCon.svFilters[2].Q);

        }
        else
        {
            filterBass.frequency(mCon.tuningFilters[0].fC);
            filterBass.resonance(mCon.tuningFilters[0].Q);
            filterMidTreb.frequency(mCon.tuningFilters[1].fC);
            filterMidTreb.resonance(mCon.tuningFilters[1].Q);
        }

        myTuner.tuning_scope=mCon.tune_scope;
        myTuner.minMatchedSamples=mCon.minSamplePairs;
        myTuner.minSamplePairs=mCon.minMatchedSamples;



        adau1372.inputControl(((mCon.adc_enable&1)==1),((mCon.adc_enable&2)==2),((mCon.adc_enable&4)==4),false);
        adau1372.inputLevel(0,mCon.adc_levels[0]);
        adau1372.inputLevel(1,mCon.adc_levels[1]);
        adau1372.inputLevel(2,mCon.adc_levels[2]);
        adau1372.outputLevel(0,mCon.dac_level);
        adau1372.outputLevel(1,mCon.dac_level);
        adau1372.pgaEnable(0,(mCon.pga_enable&1)==1);
        adau1372.pgaEnable(1,(mCon.pga_enable&2)==2);
        adau1372.pgaEnable(2,(mCon.pga_enable&4)==4);
        adau1372.pgaEnable(3,(mCon.pga_enable&8)==8);
        adau1372.pgaBoost(0,(mCon.pga_enable&16)==16);
        adau1372.pgaBoost(1,(mCon.pga_enable&32)==32);
        adau1372.pgaBoost(2,(mCon.pga_enable&64)==64);
        adau1372.pgaBoost(3,(mCon.pga_enable&128)==128);
        adau1372.pgaLevel(0,mCon.pga_levels[0]);
        adau1372.pgaLevel(1,mCon.pga_levels[1]);
        adau1372.pgaLevel(2,mCon.pga_levels[2]);
        adau1372.micBias(0,(mCon.adc_enable&~15)>>4);



        /*  applyVolume();
        applyB2f();
        applyBf2m();
        applyBass();
        applyMids();
        applyTreb(); */







  return eeA;
}//  unsigned int getPromData()

unsigned int putPromData()
{
        unsigned int eeA=profileMode*(sizeof(mCon)+2); // clearance of 1 byte between data blocks, yep, I wasted a byte.
        uint8_t nnn=(3+1 +2 +2 +(1|2|4|8|16|32|64));
        if(EEPROM.read(eeA)!=nnn) EEPROM.put(eeA,nnn);
        eeA+=sizeof(nnn);
        EEPROM.put(eeA,mCon);
        eeA+=sizeof(mCon);
        return eeA;
}//  unsigned int putPromData()





        const char cmds[]="sec\0id\0ver\0get \0jet \0set \0peaks\0loadall\0saveall\0un9396\0\0";
        typedef enum {cmd_sec=1,cmd_id,cmd_ver,cmd_get,cmd_jet,cmd_set,cmd_peaks,cmd_loadall,cmd_saveall,cmd_wipeall} cons_cmd_t;

        const char cmd1[]="options\0silly\0maxprofs\0level\0filter\0svfilt\0codec \0virtcent\0ramprate\0serial\0profile\0battsplit\0battlow\0battdead\0battmax\0label\0tunerpair\0tunermatch\0tunescope\0\0";
        typedef enum {cm1_options=1,cm1_silly,cm1_maxprofs,cm1_level,cm1_filter,cm1_svfilt,cm1_codec,cm1_virtcent,cm1_ramprate,cm1_serial,cm1_profile,cm1_battsplit,cm1_battlow,cm1_battdead,cm1_battmax,cm1_label,cm1_tunerpair,cm1_tunermatch,cm1_tunescope} cons_cm1_t;





        const char cmdc[]="adclev\0daclev\0adcen\0pgaen\0pgalev\0pga10+\0micbias\0\0";
        typedef enum {cmc_adclev=1,cmc_daclev,cmc_adcen,cmc_pgaen,cmc_pgalev,cmc_pga10p,cmc_micbias} cons_cmc_t;





        const char cmdf[]="t\0f\0q\0d\0\0";
        typedef enum {cmf_t=1,cmf_f,cmf_q,cmf_d} cons_cmf_t;


        const char cmdp[]="l\0h\0\0";
        typedef enum {cmp_l=1,cmp_h} cons_cmp_t;



    //----------------------------------------------------------------------------- Console Parser  -------------------------------------------------------------------

void consoleParser(const char* line)
{
        uint8_t ptr=0,num=0,set1=0;

        uint8_t *line1;

        bool report,validated=false;
        float float1;
        double double1;
        while(line[ptr]!=0)
        {
            report=true;
            switch(lineParse(&line[ptr],&ptr,cmds))
            {
                case cmd_sec:
                    Serial.println("se6386"); // Dalgleish, last four digits of childhood phone number.
                    validated=true;
                break;
                case cmd_id:

                    Serial.printf("id:%s 3 Way\n",labelStart);



                    validated=true;
                break;
                case cmd_ver:
                    Serial.printf("ver:%u.%u.%u\n",1,2,2);
                    validated=true;
                break;

                case cmd_get:
                    switch(lineParse(&line[ptr],&ptr,cmd1))
                    {
                        case cm1_options:
                            Serial.printf("set options %u\n",(1|2|4|8|16|32|64));
                            validated=true;
                            break;
                        case cm1_silly:
                            Serial.printf("sizeof(float)=%u\n",sizeof(float1));
                            Serial.printf("sizeof(double)=%u\n",sizeof(double1));
                            validated=true;
                            break;
                        case cm1_maxprofs:
                            Serial.printf("set maxprofs: %u\n",maxProfiles);
                            validated=true;
                            break;
                        case cm1_level:
                            num=lineParse(&line[ptr],&ptr);
                            Serial.printf("set level %u",num);
                            validated=true;
                            switch(lineParse(&line[ptr],&ptr,cmdp))
                            {
                                case cmp_l:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf("l%.1f\n",mCon.bridgeLevel[0]);
                                            break;
                                        case 1: // face
                                            Serial.printf("l%.1f\n",mCon.faceLevel[0]);
                                            break;
                                        case 2: // brfa
                                            Serial.printf("l%.1f\n",mCon.brfaLevel[0]);
                                            break;
                                        case 3: // mic
                                            Serial.printf("l%.1f\n",mCon.micLevel[0]);
                                            break;
                                        case 4: // output
                                            Serial.printf("l%.1f\n",mCon.outputLevel[0]);
                                            break;
                                        case 5: // bass
                                            Serial.printf("l%.1f\n",mCon.bassLevel[0]);
                                            break;
                                        case 6: // mids
                                            Serial.printf("l%.1f\n",mCon.midsLevel[0]);
                                            break;
                                        case 7: // treb
                                            Serial.printf("l%.1f\n",mCon.trebLevel[0]);
                                    }
                                    break;
                                case cmp_h:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf("h%.1f\n",mCon.bridgeLevel[1]);
                                            break;
                                        case 1: // face
                                            Serial.printf("h%.1f\n",mCon.faceLevel[1]);
                                            break;
                                        case 2: // brfa
                                            Serial.printf("h%.1f\n",mCon.brfaLevel[1]);
                                            break;
                                        case 3: // mic
                                            Serial.printf("h%.1f\n",mCon.micLevel[1]);
                                            break;
                                        case 4: // output
                                            Serial.printf("h%.1f\n",mCon.outputLevel[1]);
                                            break;
                                        case 5: // bass
                                            Serial.printf("h%.1f\n",mCon.bassLevel[1]);
                                            break;
                                        case 6: // mids
                                            Serial.printf("h%.1f\n",mCon.midsLevel[1]);
                                            break;
                                        case 7: // treb
                                            Serial.printf("h%.1f\n",mCon.trebLevel[1]);
                                    }
                                    break;
                                default:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf(",%.1f,%.1f\n",mCon.bridgeLevel[0],mCon.bridgeLevel[1]);
                                            break;
                                        case 1: // face
                                            Serial.printf(",%.1f,%.1f\n",mCon.faceLevel[0],mCon.faceLevel[1]);
                                            break;
                                        case 2: // brfa
                                            Serial.printf(",%.1f,%.1f\n",mCon.brfaLevel[0],mCon.brfaLevel[1]);
                                            break;
                                        case 3: // mic
                                            Serial.printf(",%.1f,%.1f\n",mCon.micLevel[0],mCon.micLevel[1]);
                                            break;
                                        case 4: // output
                                            Serial.printf(",%.1f,%.1f\n",mCon.outputLevel[0],mCon.outputLevel[1]);
                                            break;
                                        case 5: // bass
                                            Serial.printf(",%.1f,%.1f\n",mCon.bassLevel[0],mCon.bassLevel[1]);
                                            break;
                                        case 6: // mids
                                            Serial.printf(",%.1f,%.1f\n",mCon.midsLevel[0],mCon.midsLevel[1]);
                                            break;
                                        case 7: // treb
                                            Serial.printf(",%.1f,%.1f\n",mCon.trebLevel[0],mCon.trebLevel[1]);
                                    }
                            }
                            break; // cml_level

                        case cm1_filter:
                            num=lineParse(&line[ptr],&ptr);
                            set1=lineParse(&line[ptr],&ptr);
                            Serial.printf("set filter %u,%u",num,set1);
                            validated=true;
                            switch(lineParse(&line[ptr],&ptr,cmdf))
                            {
                                case cmf_t:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf("t%u\n",mCon.bridgeFilters[set1].type);
                                            break;
                                        case 1: // face
                                            Serial.printf("t%u\n",mCon.faceFilters[set1].type);
                                            break;
                                        case 2: // brfa
                                            Serial.printf("t%u\n",mCon.brfaFilters[set1].type);
                                            break;
                                        case 3: // mic
                                            Serial.printf("t%u\n",mCon.micFilters[set1].type);
                                            break;
                                        default:
                                       // case 5: // final
                                            Serial.printf("t%u\n",mCon.finalFilters[set1].type);
                                    }
                                    break;
                                case cmf_f:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf("f%.2f\n",mCon.bridgeFilters[set1].fC);
                                            break;
                                        case 1: // face
                                            Serial.printf("f%.2f\n",mCon.faceFilters[set1].fC);
                                            break;
                                        case 2: // brfa
                                            Serial.printf("f%.2f\n",mCon.brfaFilters[set1].fC);
                                            break;
                                        case 3: // mic
                                            Serial.printf("f%.2f\n",mCon.micFilters[set1].fC);
                                            break;
                                        default:
                                        // case 5: // final
                                            Serial.printf("f%.2f\n",mCon.finalFilters[set1].fC);
                                    }
                                    break;
                                case cmf_q:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf("q%.4f\n",mCon.bridgeFilters[set1].Q);
                                            break;
                                        case 1: // face
                                            Serial.printf("q%.4f\n",mCon.faceFilters[set1].Q);
                                            break;
                                        case 2: // brfa
                                            Serial.printf("q%.4f\n",mCon.brfaFilters[set1].Q);
                                            break;
                                        case 3: // mic
                                            Serial.printf("q%.4f\n",mCon.micFilters[set1].Q);
                                            break;
                                        default:
                                       // case 5: // final
                                            Serial.printf("q%.4f\n",mCon.finalFilters[set1].Q);
                                    }
                                    break;
                                case cmf_d:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf("d%.2f\n",mCon.bridgeFilters[set1].dB);
                                            break;
                                        case 1: // face
                                            Serial.printf("d%.2f\n",mCon.faceFilters[set1].dB);
                                            break;
                                        case 2: // brfa
                                            Serial.printf("d%.2f\n",mCon.brfaFilters[set1].dB);
                                            break;
                                        case 3: // mic
                                            Serial.printf("d%.2f\n",mCon.micFilters[set1].dB);
                                            break;
                                        default:
                                        // case 5: // final
                                            Serial.printf("d%.2f\n",mCon.finalFilters[set1].dB);
                                    }
                                    break;
# 1978 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                default:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.bridgeFilters[set1].type,mCon.bridgeFilters[set1].fC,mCon.bridgeFilters[set1].Q,mCon.bridgeFilters[set1].dB);
                                            break;
                                        case 1: // face
                                            Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.faceFilters[set1].type,mCon.faceFilters[set1].fC,mCon.faceFilters[set1].Q,mCon.faceFilters[set1].dB);
                                            break;
                                        case 2: // brfa
                                            Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.brfaFilters[set1].type,mCon.brfaFilters[set1].fC,mCon.brfaFilters[set1].Q,mCon.brfaFilters[set1].dB);
                                            break;
                                        case 3: // mic
                                            Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.micFilters[set1].type,mCon.micFilters[set1].fC,mCon.micFilters[set1].Q,mCon.micFilters[set1].dB);
                                            break;
                                        default:
                                        // case 5: // final
                                            Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.finalFilters[set1].type,mCon.finalFilters[set1].fC,mCon.finalFilters[set1].Q,mCon.finalFilters[set1].dB);
                                    }
                            }
                            break;

                        case cm1_svfilt:
                            num=lineParse(&line[ptr],&ptr); // 0 - highpass, 1 - tonepass_low, 2 - tonepass_high, 3 - tunepass_low, 4 - tunepass_high
                            Serial.printf("set svfilt %u",num);
                            switch(lineParse(&line[ptr],&ptr,cmdf))
                            {
                                case cmf_f:
                                    if(num<3)
                                    {
                                        Serial.printf("f %.2f\n", mCon.svFilters[num].fC);
                                        validated=true;

                                    }
                                    else
                                    {
                                        Serial.printf("f %.2f\n", mCon.tuningFilters[num-3].fC);
                                        validated=true;

                                    }
                                    break;
                                case cmf_q:
                                    if(num<3)
                                    {
                                        Serial.printf("q %.4f\n", mCon.svFilters[num].Q);
                                        validated=true;

                                    }
                                    else
                                    {
                                        Serial.printf("q %.4f\n", mCon.tuningFilters[num-3].Q);
                                        validated=true;

                                    }
                                    break;
                                default:
                                    if(num<3)
                                    {
                                        Serial.printf(",%.2f,%.4f\n", mCon.svFilters[num].fC,mCon.svFilters[num].Q);
                                        validated=true;

                                    }
                                    else
                                    {
                                        Serial.printf(",%.2f,%.4f\n", mCon.tuningFilters[num-3].fC,mCon.tuningFilters[num-3].Q);
                                        validated=true;

                                    }
                            }
                            if (!validated)
                            {
                                Serial.println("Oops");
                                validated=true;
                            }
                            break;
                        case cm1_codec:
                            switch(lineParse(&line[ptr],&ptr,cmdc))
                            {
                                case cmc_adclev:
                                    num=lineParse(&line[ptr],&ptr);
                                    Serial.printf("set codec adclev %u,%u\n",num,mCon.adc_levels[num]);
                                    validated=true;
                                    break;
                                case cmc_daclev:
                                    Serial.printf("set codec daclev %u\n",mCon.dac_level);
                                    validated=true;
                                    break;
                                case cmc_adcen:
                                    Serial.printf("set codec adcen %u\n",mCon.adc_enable&15);
                                    validated=true;
                                    break;
                                case cmc_pgaen:
                                    Serial.printf("set codec pgaen %u\n",mCon.pga_enable&15);
                                    validated=true;
                                    break;
                                case cmc_pgalev:
                                    num=lineParse(&line[ptr],&ptr);
                                    Serial.printf("set codec pgalev %u,%u\n",num,mCon.pga_levels[num]);
                                    validated=true;
                                    break;
                                case cmc_pga10p:
                                    Serial.printf("set codec pga10+ %u\n",(mCon.pga_enable&~15)>>4);
                                    validated=true;
                                    break;
                                case cmc_micbias:
                                    Serial.printf("set codec micbias %u\n",(mCon.adc_enable&~15)>>4);
                                    validated=true;

                            }
                            break;
                        case cm1_virtcent:
                            Serial.printf("set virtcent %.2f\n",mCon.virtualCenter);
                            validated=true;
                            break;
                        case cm1_ramprate:
                            Serial.printf("set ramprate %.2f\n",mCon.rampRate);
                            validated=true;
                            break;
                        case cm1_serial:
                            Serial.printf("set serial %u %u\n",mSer.seryr,mSer.serhr);
                            validated=true;
                            break;
                        case cm1_profile:
                            Serial.printf("set profile %u\n",profileMode);
                            validated=true;
                            break;
                        case cm1_battsplit:
                            Serial.printf("set battsplit %f\n",mCon.battery_switch);
                            validated=true;
                            break;
                        case cm1_battlow:
                            num=(uint8_t)lineParse(&line[ptr],&ptr);
                            Serial.printf("set battlow %u,%f\n",num,mCon.battery_low[num]);
                            validated=true;

                            break;
                        case cm1_battdead:
                            num=(uint8_t)lineParse(&line[ptr],&ptr);
                            Serial.printf("set battdead %u,%f\n",num,mCon.battery_dead[num]);
                            validated=true;
                            break;
                        case cm1_battmax:
                            num=(uint8_t)lineParse(&line[ptr],&ptr);
                            Serial.printf("set battmax %u,%f\n",num,mCon.battery_max[num]);
                            validated=true;
                            break;
                        case cm1_label:
                            switch((uint8_t)lineParse(&line[ptr],&ptr))
                            {
                                case 1:
                                    Serial.printf("set label 1,%s\n",mCon.face_label);
                                    validated=true;
                                    break;
                                default: // case 2:
                                    Serial.printf("set label 2,%s\n",mCon.mic_label);
                                    validated=true;
                            }
                            break;
                        case cm1_tunerpair:
                            Serial.printf("set tunerpair %u\n",mCon.minSamplePairs);
                            validated=true;
                            break;
                        case cm1_tunermatch:
                            Serial.printf("set tunermatch %u\n",mCon.minMatchedSamples);
                            validated=true;
                            break;
                        case cm1_tunescope:
                            Serial.printf("set tunescope %f\n",mCon.tune_scope*10);
                            validated=true;

                    } // switch(lineParse(&line[ptr],&ptr,cmd1))
                    break;
                case cmd_jet:
                    Serial.println("jet: set command not reported.");
                    validated=true;
                    report=false;
                case cmd_set:
                    switch(lineParse(&line[ptr],&ptr,cmd1))
                    {
                        case cm1_options: // read only.
                            Serial.printf("set options %u\n",(1|2|4|8|16|32|64));
                            validated=true;
                            break;
                        case cm1_maxprofs: // read only.
                            Serial.printf("set maxprofs: %u\n",maxProfiles);
                            validated=true;
                            break;
                        case cm1_level:
                            num=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let level %u",num);
                            validated=true;

                            switch(lineParse(&line[ptr],&ptr,cmdp))
                            {
                                case cmp_l:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            mCon.bridgeLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.bridgeLevel[0]);
                                            break;
                                        case 1: // face
                                            mCon.faceLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.faceLevel[0]);
                                            break;
                                        case 2: // brfa
                                            mCon.brfaLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.brfaLevel[0]);
                                            break;
                                        case 3: // mic
                                            mCon.micLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.micLevel[0]);
                                            break;
                                        case 4: // output
                                            mCon.outputLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.outputLevel[0]);
                                            break;
                                        case 5: // bass
                                            mCon.bassLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.bassLevel[0]);
                                            break;
                                        case 6: // mids
                                            mCon.midsLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.midsLevel[0]);
                                            break;
                                        case 7: // treb
                                            mCon.trebLevel[0]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("l%.1f\n",mCon.trebLevel[0]);
                                    }
                                    break;
                                case cmp_h:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            mCon.bridgeLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.bridgeLevel[1]);
                                            break;
                                        case 1: // face
                                            mCon.faceLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.faceLevel[1]);
                                            break;
                                        case 2: // brfa
                                            mCon.brfaLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.brfaLevel[1]);
                                            break;
                                        case 3: // mic
                                            mCon.micLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.micLevel[1]);
                                            break;
                                        case 4: // output
                                            mCon.outputLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.outputLevel[1]);
                                            break;
                                        case 5: // bass
                                            mCon.bassLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.bassLevel[1]);
                                            break;
                                        case 6: // mids
                                            mCon.midsLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.midsLevel[1]);
                                            break;
                                        case 7: // treb
                                            mCon.trebLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf("h%.1f\n",mCon.trebLevel[1]);
                                    }
                                    break;
                                default:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            mCon.bridgeLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.bridgeLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.bridgeLevel[0],mCon.bridgeLevel[1]);
                                            break;
                                        case 1: // face
                                            mCon.faceLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.faceLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.faceLevel[0],mCon.faceLevel[1]);
                                            break;
                                        case 2: // brfa
                                            mCon.brfaLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.brfaLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.brfaLevel[0],mCon.brfaLevel[1]);
                                            break;
                                        case 3: // mic
                                            mCon.micLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.micLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.micLevel[0],mCon.micLevel[1]);
                                            break;
                                        case 4: // output
                                            mCon.outputLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.outputLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.outputLevel[0],mCon.outputLevel[1]);
                                            break;
                                        case 5: // bass
                                            mCon.bassLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.bassLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.bassLevel[0],mCon.bassLevel[1]);
                                            break;
                                        case 6: // mids
                                            mCon.midsLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.midsLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.midsLevel[0],mCon.midsLevel[1]);
                                            break;
                                        case 7: // treb
                                            mCon.trebLevel[0]=lineParse(&line[ptr],&ptr);
                                            mCon.trebLevel[1]=lineParse(&line[ptr],&ptr);
                                            if(report) Serial.printf(",%.1f,%.1f\n",mCon.trebLevel[0],mCon.trebLevel[1]);
                                    }
                            }
                            switch(num)
                            {
                                case 0: // bridge
                                    applyB2f();
                                    break;
                                case 1: // face
                                    applyB2f();
                                    break;
                                case 2: // brfa
                                    applyBf2m();
                                    break;
                                case 3: // mic
                                    applyBf2m();
                                    break;
                                case 4: // output
                                    applyVolume();
                                    break;
                                case 5: // bass
                                    applyBass();
                                    break;
                                case 6: // mids
                                   applyMids();
                                   break;
                                case 7: // treb
                                  applyTreb();
                            }
                            break;

                        case cm1_filter:
                            num=lineParse(&line[ptr],&ptr);
                            set1=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let filter %u,%u",num,set1);
                            validated=true;
                            switch(lineParse(&line[ptr],&ptr,cmdf))
                            {
                                case cmf_t:
                                    switch(num)
                                    {
                                        case 0: // bridge

                                            mCon.bridgeFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            if(report) Serial.printf("t%u\n",mCon.bridgeFilters[set1].type);
                                            break;
                                        case 1: // face

                                            mCon.faceFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            if(report) Serial.printf("t%u\n",mCon.faceFilters[set1].type);
                                            break;
                                        case 2: // brfa

                                            mCon.brfaFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            if(report) Serial.printf("t%u\n",mCon.brfaFilters[set1].type);
                                            break;
                                        case 3: // mic

                                            mCon.micFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            if(report) Serial.printf("t%u\n",mCon.micFilters[set1].type);
                                            break;
                                        default:
                                        // case 5: // final

                                            mCon.finalFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            if(report) Serial.printf("t%u\n",mCon.finalFilters[set1].type);
                                    }
                                    break;
                                case cmf_f:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            mCon.bridgeFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            if(mCon.bridgeFilters[set1].fC<10) mCon.bridgeFilters[set1].fC=10;
                                            if(mCon.bridgeFilters[set1].fC>23500) mCon.bridgeFilters[set1].fC=23500;
                                            if(report) Serial.printf("f%.2f\n",mCon.bridgeFilters[set1].fC);
                                            break;
                                        case 1: // face
                                            mCon.faceFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            if(mCon.faceFilters[set1].fC<10) mCon.faceFilters[set1].fC=10;
                                            if(mCon.faceFilters[set1].fC>23500) mCon.faceFilters[set1].fC=23500;
                                            if(report) Serial.printf("f%.2f\n",mCon.faceFilters[set1].fC);
                                            break;
                                        case 2: // brfa
                                            mCon.brfaFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            if(mCon.brfaFilters[set1].fC<10) mCon.brfaFilters[set1].fC=10;
                                            if(mCon.brfaFilters[set1].fC>23500) mCon.brfaFilters[set1].fC=23500;
                                            if(report) Serial.printf("f%.2f\n",mCon.brfaFilters[set1].fC);
                                            break;
                                        case 3: // mic
                                            mCon.micFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            if(mCon.micFilters[set1].fC<10) mCon.micFilters[set1].fC=10;
                                            if(mCon.micFilters[set1].fC>23500) mCon.micFilters[set1].fC=23500;
                                            if(report) Serial.printf("f%.2f\n",mCon.micFilters[set1].fC);
                                            break;
                                        default:
                                        // case 5: // final
                                            mCon.finalFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            if(mCon.finalFilters[set1].fC<10) mCon.finalFilters[set1].fC=10;
                                            if(mCon.finalFilters[set1].fC>23500) mCon.finalFilters[set1].fC=23500;
                                            if(report) Serial.printf("f%.2f\n",mCon.finalFilters[set1].fC);
                                    }
                                    break;
                                case cmf_q:
                                    switch(num)
                                   {
                                        case 0: // bridge
                                            mCon.bridgeFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            if(mCon.bridgeFilters[set1].Q<0.001) mCon.bridgeFilters[set1].Q=0.001;
                                            if(mCon.bridgeFilters[set1].Q>14.141) mCon.bridgeFilters[set1].Q=14.141;
                                            if(report) Serial.printf("q%.4f\n",mCon.bridgeFilters[set1].Q);
                                            break;
                                        case 1: // face
                                            mCon.faceFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            if(mCon.faceFilters[set1].Q<0.001) mCon.faceFilters[set1].Q=0.001;
                                            if(mCon.faceFilters[set1].Q>14.141) mCon.faceFilters[set1].Q=0.001;
                                            if(report) Serial.printf("q%.4f\n",mCon.faceFilters[set1].Q);
                                            break;
                                        case 2: // brfa
                                            mCon.brfaFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            if(mCon.brfaFilters[set1].Q<0.001) mCon.brfaFilters[set1].Q=0.001;
                                            if(mCon.brfaFilters[set1].Q>14.141) mCon.brfaFilters[set1].Q=14.141;
                                            if(report) Serial.printf("q%.4f\n",mCon.brfaFilters[set1].Q);
                                            break;
                                        case 3: // mic
                                            mCon.micFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            if(mCon.micFilters[set1].Q<0.001) mCon.micFilters[set1].Q=0.001;
                                            if(mCon.micFilters[set1].Q>14.141) mCon.micFilters[set1].Q=14.141;
                                            if(report) Serial.printf("q%.4f\n",mCon.micFilters[set1].Q);
                                            break;
                                        default:
                                        // case 5: // final
                                            mCon.finalFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            if(mCon.finalFilters[set1].Q<0.001) mCon.finalFilters[set1].Q=0.001;
                                            if(mCon.finalFilters[set1].Q>14.141) mCon.finalFilters[set1].Q=14.141;
                                            if(report) Serial.printf("q%.4f\n",mCon.finalFilters[set1].Q);
                                    }
                                    break;
                                case cmf_d:
                                    switch(num)
                                    {
                                        case 0: // bridge
                                            mCon.bridgeFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.bridgeFilters[set1].dB<-80) mCon.bridgeFilters[set1].dB=-80;
                                            if(mCon.bridgeFilters[set1].dB>10) mCon.bridgeFilters[set1].dB=10;
                                            if(report) Serial.printf("d%.2f\n",mCon.bridgeFilters[set1].dB);
                                            break;
                                        case 1: // face
                                            mCon.faceFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.faceFilters[set1].dB<-80) mCon.faceFilters[set1].dB=-80;
                                            if(mCon.faceFilters[set1].dB>10) mCon.faceFilters[set1].dB=10;
                                            if(report) Serial.printf("d%.2f\n",mCon.faceFilters[set1].dB);
                                            break;
                                        case 2: // brfa
                                            mCon.brfaFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.brfaFilters[set1].dB<-80) mCon.brfaFilters[set1].dB=-80;
                                            if(mCon.brfaFilters[set1].dB>10) mCon.brfaFilters[set1].dB=10;
                                            if(report) Serial.printf("d%.2f\n",mCon.brfaFilters[set1].dB);
                                            break;
                                        case 3: // mic
                                            mCon.micFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.micFilters[set1].dB<-80) mCon.micFilters[set1].dB=-80;
                                            if(mCon.micFilters[set1].dB>10) mCon.micFilters[set1].dB=10;
                                            if(report) Serial.printf("d%.2f\n",mCon.micFilters[set1].dB);
                                            break;
                                        default:
                                        // case 5: // final
                                            mCon.finalFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.finalFilters[set1].dB<-80) mCon.finalFilters[set1].dB=-80;
                                            if(mCon.finalFilters[set1].dB>10) mCon.finalFilters[set1].dB=10;
                                            if(report) Serial.printf("d%.2f\n",mCon.finalFilters[set1].dB);
                                    }
                                    break;
# 2520 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                default:
                                    switch(num)
                                    {
                                        case 0: // bridge

                                            mCon.bridgeFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            mCon.bridgeFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            mCon.bridgeFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            mCon.bridgeFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.bridgeFilters[set1].fC<10) mCon.bridgeFilters[set1].fC=10;
                                            if(mCon.bridgeFilters[set1].fC>23500) mCon.bridgeFilters[set1].fC=23500;
                                            if(mCon.bridgeFilters[set1].Q<0.001) mCon.bridgeFilters[set1].Q=0.001;
                                            if(mCon.bridgeFilters[set1].Q>14.141) mCon.bridgeFilters[set1].Q=14.141;
                                            if(mCon.bridgeFilters[set1].dB<-80) mCon.bridgeFilters[set1].dB=-80;
                                            if(mCon.bridgeFilters[set1].dB>10) mCon.bridgeFilters[set1].dB=10;
                                            if(report) Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.bridgeFilters[set1].type,mCon.bridgeFilters[set1].fC,mCon.bridgeFilters[set1].Q,mCon.bridgeFilters[set1].dB);
                                            break;
                                        case 1: // face

                                            mCon.faceFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            mCon.faceFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            mCon.faceFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            mCon.faceFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.faceFilters[set1].fC<10) mCon.faceFilters[set1].fC=10;
                                            if(mCon.faceFilters[set1].fC>23500) mCon.faceFilters[set1].fC=23500;
                                            if(mCon.faceFilters[set1].Q<0.001) mCon.faceFilters[set1].Q=0.001;
                                            if(mCon.faceFilters[set1].Q>14.141) mCon.faceFilters[set1].Q=14.141;
                                            if(mCon.faceFilters[set1].dB<-80) mCon.faceFilters[set1].dB=-80;
                                            if(mCon.faceFilters[set1].dB>10) mCon.faceFilters[set1].dB=10;
                                            if(report) Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.faceFilters[set1].type,mCon.faceFilters[set1].fC,mCon.faceFilters[set1].Q,mCon.faceFilters[set1].dB);
                                            break;
                                        case 2: // brfa

                                            mCon.brfaFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            mCon.brfaFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            mCon.brfaFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            mCon.brfaFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.brfaFilters[set1].fC<10) mCon.brfaFilters[set1].fC=10;
                                            if(mCon.brfaFilters[set1].fC>23500) mCon.brfaFilters[set1].fC=23500;
                                            if(mCon.brfaFilters[set1].Q<0.001) mCon.brfaFilters[set1].Q=0.001;
                                            if(mCon.brfaFilters[set1].Q>14.141) mCon.brfaFilters[set1].Q=14.141;
                                            if(mCon.brfaFilters[set1].dB<-80) mCon.brfaFilters[set1].dB=-80;
                                            if(mCon.brfaFilters[set1].dB>10) mCon.brfaFilters[set1].dB=10;
                                            if(report) Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.brfaFilters[set1].type,mCon.brfaFilters[set1].fC,mCon.brfaFilters[set1].Q,mCon.brfaFilters[set1].dB);
                                            break;
                                        case 3: // mic

                                            mCon.micFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            mCon.micFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            mCon.micFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            mCon.micFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.micFilters[set1].fC<10) mCon.micFilters[set1].fC=10;
                                            if(mCon.micFilters[set1].fC>23500) mCon.micFilters[set1].fC=23500;
                                            if(mCon.micFilters[set1].Q<0.001) mCon.micFilters[set1].Q=0.001;
                                            if(mCon.micFilters[set1].Q>14.141) mCon.micFilters[set1].Q=14.141;
                                            if(mCon.micFilters[set1].dB<-80) mCon.micFilters[set1].dB=-80;
                                            if(mCon.micFilters[set1].dB>10) mCon.micFilters[set1].dB=10;
                                            if(report) Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.micFilters[set1].type,mCon.micFilters[set1].fC,mCon.micFilters[set1].Q,mCon.micFilters[set1].dB);
                                            break;
                                        default:
                                        // case 5: // final

                                            mCon.finalFilters[set1].type=(uint8_t)lineParse(&line[ptr],&ptr)&7;



                                            mCon.finalFilters[set1].fC=lineParse(&line[ptr],&ptr);
                                            mCon.finalFilters[set1].Q=lineParse(&line[ptr],&ptr);
                                            mCon.finalFilters[set1].dB=lineParse(&line[ptr],&ptr);
                                            if(mCon.finalFilters[set1].fC<10) mCon.finalFilters[set1].fC=10;
                                            if(mCon.finalFilters[set1].fC>23500) mCon.finalFilters[set1].fC=23500;
                                            if(mCon.finalFilters[set1].Q<0.001) mCon.finalFilters[set1].Q=0.001;
                                            if(mCon.finalFilters[set1].Q>14.141) mCon.finalFilters[set1].Q=14.141;
                                            if(mCon.finalFilters[set1].dB<-80) mCon.finalFilters[set1].dB=-80;
                                            if(mCon.finalFilters[set1].dB>10) mCon.finalFilters[set1].dB=10;
                                            if(report) Serial.printf(",%u,%.2f,%.4f,%.2f\n",mCon.finalFilters[set1].type,mCon.finalFilters[set1].fC,mCon.finalFilters[set1].Q,mCon.finalFilters[set1].dB);
                                    }
                            }//  switch(lineParse()
                            applyFilter(num,set1);
                            break;

                        case cm1_svfilt:
                            num=lineParse(&line[ptr],&ptr); // 0 - highpass, 1 - tonepass_low, 2 - tonepass_high, 3 - tunepass_low, 4 - tunepass_high
                            if(report) Serial.printf("let svfilt %u",num);
                            validated=true;
                            switch(lineParse(&line[ptr],&ptr,cmdf))
                            {
                                case cmf_f:
                                    if(num<3)
                                    {
                                        mCon.svFilters[num].fC=lineParse(&line[ptr],&ptr);
                                        if(mCon.svFilters[num].fC<10) mCon.svFilters[num].fC=10;
                                        if(mCon.svFilters[num].fC>23500) mCon.svFilters[num].fC=23500;
                                        switch(num)
                                        {
                                            case 0:
                                                filter1.frequency(mCon.svFilters[num].fC);
                                                break;
                                            case 1:
                                                if(mainMode==preamp) filterBass.frequency(mCon.svFilters[num].fC);
                                                break;
                                            case 2:
                                                if(mainMode==preamp) filterMidTreb.frequency(mCon.svFilters[num].fC);
                                        }
                                        if(report) Serial.printf("f %.2f\n", mCon.svFilters[num].fC);

                                    }
                                    else
                                    {
                                        mCon.tuningFilters[num-3].fC=lineParse(&line[ptr],&ptr);
                                        if(mCon.tuningFilters[num-3].fC<10) mCon.tuningFilters[num-3].fC=10;
                                        if(mCon.tuningFilters[num-3].fC>23500) mCon.tuningFilters[num-3].fC=23500;
                                        if(mainMode==tuner)
                                        {
                                            if(num==3)
                                            {
                                                filterBass.frequency(mCon.tuningFilters[num].fC);
                                            }
                                            else
                                            {
                                                filterMidTreb.frequency(mCon.tuningFilters[num].fC);
                                            }
                                        }
                                        if(report) Serial.printf("f %.2f\n", mCon.tuningFilters[num-3].fC);

                                    }
                                    break;
                                case cmf_q:
                                    if(num<3)
                                    {
                                        mCon.svFilters[num].Q=lineParse(&line[ptr],&ptr);
                                        if(mCon.svFilters[num].Q<0.001) mCon.svFilters[num].Q=0.001;
                                        if(mCon.svFilters[num].Q>14.141) mCon.svFilters[num].Q=14.141;
                                        switch(num)
                                        {
                                            case 0:
                                                filter1.resonance(mCon.svFilters[num].Q);
                                                break;
                                            case 1:
                                                if(mainMode==preamp) filterBass.resonance(mCon.svFilters[num].Q);
                                                break;
                                            case 2:
                                                if(mainMode==preamp) filterMidTreb.resonance(mCon.svFilters[num].Q);
                                        }
                                        if(report) Serial.printf("q %.4f\n", mCon.svFilters[num].Q);

                                    }
                                    else
                                    {
                                        mCon.tuningFilters[num-3].Q=lineParse(&line[ptr],&ptr);
                                        if(mCon.tuningFilters[num-3].Q<0.001) mCon.tuningFilters[num-3].Q=0.001;
                                        if(mCon.tuningFilters[num-3].Q>14.141) mCon.tuningFilters[num-3].Q=14.141;
                                        if(mainMode==tuner)
                                        {
                                            if(num==3)
                                            {
                                                filterBass.resonance(mCon.tuningFilters[num].Q);
                                            }
                                            else
                                            {
                                                filterMidTreb.resonance(mCon.tuningFilters[num].Q);
                                            }
                                        }
                                        if(report) Serial.printf("q %.4f\n", mCon.tuningFilters[num-3].Q);

                                    }
                                    break;
                                default:
                                    if(num<3)
                                    {
                                        mCon.svFilters[num].fC=lineParse(&line[ptr],&ptr);
                                        mCon.svFilters[num].Q=lineParse(&line[ptr],&ptr);
                                        if(mCon.svFilters[num].fC<10) mCon.svFilters[num].fC=10;
                                        if(mCon.svFilters[num].fC>23500) mCon.svFilters[num].fC=23500;
                                        if(mCon.svFilters[num].Q<0.001) mCon.svFilters[num].Q=0.001;
                                        if(mCon.svFilters[num].Q>14.141) mCon.svFilters[num].Q=14.141;

                                        switch(num)
                                        {
                                            case 0:
                                                filter1.frequency(mCon.svFilters[num].fC);
                                                filter1.resonance(mCon.svFilters[num].Q);
                                                break;
                                            case 1:
                                                if(mainMode==preamp)
                                                {
                                                    filterBass.frequency(mCon.svFilters[num].fC);
                                                    filterBass.resonance(mCon.svFilters[num].Q);
                                                }
                                                break;
                                            case 2:
                                                if(mainMode==preamp)
                                                {
                                                    filterMidTreb.frequency(mCon.svFilters[num].fC);
                                                    filterMidTreb.resonance(mCon.svFilters[num].Q);
                                                }
                                        }
                                        if(report) Serial.printf(",%.2f,%.4f\n", mCon.svFilters[num].fC,mCon.svFilters[num].Q);

                                    }
                                    else
                                    {
                                        mCon.tuningFilters[num-3].fC=lineParse(&line[ptr],&ptr);
                                        mCon.tuningFilters[num-3].Q=lineParse(&line[ptr],&ptr);
                                        if(mCon.tuningFilters[num-3].fC<10) mCon.tuningFilters[num-3].fC=10;
                                        if(mCon.tuningFilters[num-3].fC>23500) mCon.tuningFilters[num-3].fC=23500;
                                        if(mCon.tuningFilters[num-3].Q<0.001) mCon.tuningFilters[num-3].Q=0.001;
                                        if(mCon.tuningFilters[num-3].Q>14.141) mCon.tuningFilters[num-3].Q=14.141;

                                        if(mainMode==tuner)
                                        {
                                            if(num==3)
                                            {
                                                filterBass.frequency(mCon.tuningFilters[num].fC);
                                                filterBass.resonance(mCon.tuningFilters[num].Q);
                                            }
                                            else
                                            {
                                                filterMidTreb.frequency(mCon.tuningFilters[num].fC);
                                                filterMidTreb.resonance(mCon.tuningFilters[num].Q);
                                            }
                                        }
                                        if(report) Serial.printf(",%.2f,%.4f\n", mCon.tuningFilters[num-3].fC,mCon.tuningFilters[num-3].Q);

                                    }
                            }//  switch(lineParse(&line[ptr],&ptr,cmdf))
                            break;
                        case cm1_codec: // ouch
                            switch(lineParse(&line[ptr],&ptr,cmdc))
                            {
                                case cmc_adclev:
                                    num=lineParse(&line[ptr],&ptr);
                                    mCon.adc_levels[num]=lineParse(&line[ptr],&ptr);
                                    if(report) Serial.printf("let codec adclev %u,%u\n",num,mCon.adc_levels[num]);
                                    validated=true;
                                    adau1372.inputLevel(num,mCon.adc_levels[num]);
                                    break;
                                case cmc_daclev:
                                    mCon.dac_level=lineParse(&line[ptr],&ptr);
                                    if(report) Serial.printf("let codec daclev %u\n",mCon.dac_level);
                                    validated=true;
                                    adau1372.outputLevel(0,mCon.dac_level);
                                    break;
                                case cmc_adcen:
                                    mCon.adc_enable=(mCon.adc_enable&~15)|(((uint8_t)lineParse(&line[ptr],&ptr))&15);
                                    if(report) Serial.printf("let codec adcen %u\n",mCon.adc_enable&15);
                                    validated=true;
                                    adau1372.inputControl(((mCon.adc_enable&1)==1),((mCon.adc_enable&2)==2),((mCon.adc_enable&4)==4),false);
                                    break;
                                case cmc_pgaen:
                                    mCon.pga_enable=(mCon.pga_enable&~15)|(((uint8_t)lineParse(&line[ptr],&ptr))&15);
                                    if(report) Serial.printf("let codec pgaen %u\n",mCon.pga_enable&15);
                                    validated=true;
                                    adau1372.pgaEnable(0,(mCon.pga_enable&1)==1);
                                    adau1372.pgaEnable(1,(mCon.pga_enable&2)==2);
                                    adau1372.pgaEnable(2,(mCon.pga_enable&4)==4);
                                    adau1372.pgaEnable(3,(mCon.pga_enable&8)==8);
                                    break;
                                case cmc_pgalev:
                                    num=lineParse(&line[ptr],&ptr);
                                    mCon.pga_levels[num]=(uint8_t)lineParse(&line[ptr],&ptr);
                                    if(report) Serial.printf("let codec pgalev %u,%u\n",num,mCon.pga_levels[num]);
                                    validated=true;
                                    adau1372.pgaLevel(num,mCon.pga_levels[num]);
                                    break;
                                case cmc_pga10p:
                                    mCon.pga_enable=(mCon.pga_enable&15)|((((uint8_t)lineParse(&line[ptr],&ptr))&15)<<4);
                                    if(report) Serial.printf("let codec pga10+ %u\n",(mCon.pga_enable&~15)>>4);
                                    validated=true;
                                    adau1372.pgaBoost(0,(mCon.pga_enable&16)==16);
                                    adau1372.pgaBoost(1,(mCon.pga_enable&32)==32);
                                    adau1372.pgaBoost(2,(mCon.pga_enable&64)==64);
                                    adau1372.pgaBoost(3,(mCon.pga_enable&128)==128);
                                    break;
                                case cmc_micbias:
                                    mCon.adc_enable=(mCon.adc_enable&15)|(((uint8_t)lineParse(&line[ptr],&ptr)&15)<<4);
                                    if(report) Serial.printf("let codec micbias %u\n",(mCon.adc_enable&~15)>>4);
                                    validated=true;
                                    adau1372.micBias(0,(mCon.adc_enable&~15)>>4);

                            }
                            break;

                        case cm1_virtcent:
                            mCon.virtualCenter=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let virtcent %.2f\n",mCon.virtualCenter);
                            validated=true;
                            applyVolume();
                            break;
                        case cm1_ramprate:
                            mCon.rampRate=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let ramprate %.2f\n",mCon.rampRate);
                            validated=true;
                            break;
                        case cm1_serial:
                            if (mSer.seryr==0) mSer.seryr=ulongParse(&line[ptr],&ptr); else ulongParse(&line[ptr],&ptr);
                            if (mSer.serhr==0) mSer.serhr=lineParse(&line[ptr],&ptr); else lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let serial %u %u\n",mSer.seryr,mSer.serhr);
                            validated=true;
                            setSerialData(mSer);
                            break;
                        case cm1_profile:
                            num=lineParse(&line[ptr],&ptr);
                            profileMode=num;
                            // Serial.println("Calling getPromData();");
                            getPromData();
                            // Serial.println("returned from getPromData()");
                            if(report) Serial.printf("let profile %u\n",profileMode);
                            validated=true;

                            break;
                        case cm1_battsplit:
                            mCon.battery_switch=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let battsplit %f\n",mCon.battery_switch);
                            validated=true;
                            break;
                        case cm1_battlow:
                            num=(uint8_t)lineParse(&line[ptr],&ptr);
                            mCon.battery_low[num]=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let battlow %u,%f\n",num,mCon.battery_low[num]);
                            validated=true;

                            break;
                        case cm1_battdead:
                            num=(uint8_t)lineParse(&line[ptr],&ptr);
                            mCon.battery_dead[num]=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let battdead %u,%f\n",num,mCon.battery_dead[num]);
                            validated=true;
                            break;
                        case cm1_battmax:
                            num=(uint8_t)lineParse(&line[ptr],&ptr);
                            mCon.battery_max[num]=lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let battmax %u,%f\n",num,mCon.battery_max[num]);
                            validated=true;
                            break;
                        case cm1_label:
                            switch((uint8_t)lineParse(&line[ptr],&ptr))
                            {
                                case 1: // the face label
                                    ptr++;

                                    set1=ptr;
                                    num=0;
                                    while ((line[set1]>31)&&num<8)
                                    {
                                        set1++;
                                        num++;
                                    }
                                    line1=(uint8_t*)&line[set1];
                                    *line1=0;

                                    strcpy((char*)mCon.face_label,&line[ptr]);
                                    if(report) Serial.printf("let label 1,%s\n",mCon.face_label);
                                    validated=true;
                                    dispflags|=15;
                                    break;
                                default:
                                // case 2: // the mic label
                                    ptr++;
                                    set1=ptr;
                                    num=0;
                                    while ((line[set1]>31)&&num<8)
                                    {
                                        set1++;
                                        num++;
                                    }
                                    line1=(uint8_t*)&line[set1];
                                    *line1=0;
                                    strcpy((char*)mCon.mic_label,&line[ptr]); // important - must be last item on line for sure!
                                    if(report) Serial.printf("let label 2,%s\n",mCon.mic_label);
                                    validated=true;
                                    dispflags|=15;
                            }
                            break;
                        case cm1_tunerpair:
                            mCon.minSamplePairs=(uint8_t)lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let tunerpair %u\n",mCon.minSamplePairs);
                            validated=true;
                            myTuner.minSamplePairs=mCon.minSamplePairs;
                            break;
                        case cm1_tunermatch:
                            mCon.minMatchedSamples=(uint8_t)lineParse(&line[ptr],&ptr);
                            if(report) Serial.printf("let tunermatch %u\n",mCon.minMatchedSamples);
                            validated=true;
                            myTuner.minMatchedSamples=mCon.minMatchedSamples;
                            break;
                        case cm1_tunescope:
                            mCon.tune_scope=lineParse(&line[ptr],&ptr)/10;
                            if(report) Serial.printf("let tunescope %f\n",mCon.tune_scope*10);
                            validated=true;
                            myTuner.tuning_scope=mCon.tune_scope;

                    } // switch(lineParse(&line[ptr],&ptr,cmd1))

                    break;
                case cmd_peaks:
                    if(peakBfm.available()) { Serial.printf("bfm %f ",peakBfm.read()); } else { Serial.print("bfm 0 "); }
                    if(peakOut.available()) { Serial.printf("out %f ",peakOut.read()); } else { Serial.print("out 0 "); }
                    Serial.print("AudioProcessorUsageMax: ");
                    Serial.print(((((AudioStream::cpu_cycles_total_max) + (120000000 / 32 / 48000 * 128 / 100)) / (120000000 / 16 / 48000 * 128 / 100))),10);
                    Serial.print(" AudioMemoryUsageMax: ");
                    Serial.print((AudioStream::memory_used_max),10);
                    Serial.print(" AudioProcessorUsage: ");
                    Serial.print(((((AudioStream::cpu_cycles_total) + (120000000 / 32 / 48000 * 128 / 100)) / (120000000 / 16 / 48000 * 128 / 100))),10);
                    Serial.print(" Battery: ");
                    Serial.print(batteryReading,10);
                    Serial.println("V");
                    validated=true;
                    break;
                case cmd_loadall:
                    Serial.printf("loadall %u\n",getPromData());
                    validated=true;
                    break;
                case cmd_saveall:
                    Serial.printf("saveall %u\n",putPromData());
                    validated=true;
                    break;
                case cmd_wipeall:
                    Serial.println("Required: Reprogramming from scratch. Good-bye.");
                    validated=true;
                    // sDat.validToken=~EEPROMDATAVALIDTOKEN;
                    // EEPROM.put(1990,sDat);
                    flashQuickUnlockBits();
                    while(1);
                    break;
                default:
                    ptr++;
            } // switch(lineParse(&line[ptr],&ptr,cmds))
  } // while(line[ptr]!=0)
  if(!validated) Serial.println("No valid command.");
} // void consoleParser(const char* line)


    //----------------------------------------------------------------------------- Console Parser  -------------------------------------------------------------------


    //----------------------------------------------------------------------------- OLED  -------------------------------------------------------------------


void latchUp()
{
        digitalWrite(12,1); // make sure latch is on.
        dispflags|=64;
}//  End  void latchUp()

void latchDown()
{
        digitalWrite(12,0); // make sure latch is off.
        mainMode=isoffactually;
        dispflags&=~64;
}//  End  void latchDown()


void displayUp()
{
        display_timeout=0;
        if((dispflags&128)!=0) return;
        display.ssd1306_command(0xAF);
        dispflags|=128;
}//  End  void displayUp()

void displayDown()
{
        if((dispflags&128)==0) return;
        //  if(runLevel&128) runLevel&=~128;
        if((dispflags&15)==dispLogo)
        {
            checkDisplay(dispLogo2);
            display.display();
            displayUp();
            display_timeout=5000 -3000;
            return;
        }
        else
        if((dispflags&15)==dispLogo2)
        {
            checkDisplay(dispProfile);
            display.display();
            displayUp();
            display_timeout=5000 -3000;
            return;
        }
        display.ssd1306_command(0xAE);
        dispflags&=~128;
}//  End  void displayDown()

void redoDialV(float value, float minValue, float maxValue, uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
        display.fillRect(x+1,y+1,w-2,h-2,0);
        display.drawRect(x,y,w,h,1);
        if(value<minValue) value=minValue;
        if(value>maxValue) value=maxValue;
        float h1=(h-4)*((value-minValue)/(maxValue-minValue));
        float y1=(y+h-2)-h1;
        display.fillRect(x+2,y1,w-4,h1,1);
}//  End  void redoDialV()

void redoDialH(float value, float minValue, float maxValue, uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
  display.fillRect(x+1,y+1,w-2,h-2,0);
  display.drawRect(x,y,w,h,1);
  if(value<minValue) value=minValue;
  if(value>maxValue) value=maxValue;
  float w1=(w-4)*((value-minValue)/(maxValue-minValue));
  display.fillRect(x+2,y+2,w1,h-4,1);
}

void doDialPipsV(uint8_t x, uint8_t y, uint8_t w, uint8_t h, bool asTone)
{
        float hn=((float)h-4)/10;
        float yn=(float)y+h-(1+hn);
        for(uint8_t n=1;n<11;n++)
        {
            if(asTone&&n==5)
            {
                display.drawLine(0,yn,127,yn,1);
            }
            else
            {
                display.drawLine(x-n,yn,x+w+n-1,yn,1);
            }
            yn-=hn;
        }
}// End void doDialPipsV()

void doDialPipsH(uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
        float wn=((float)w-4)/10;
        float xn=(float)x+1+wn;
        for(uint8_t n=1;n<11;n++)
        {
            display.drawLine(xn,y-n,xn,y+h+n-1,1);
            display.drawLine(xn-1,y-n,xn-1,y+h+n-1,1);
            xn+=wn;
        }
}//End void doDialPipsH()

void tunerDisplay()
{
        display.clearDisplay();
        uint8_t tnote=0,toffs;
        float x; //,w;
        x=(display.width()/2)-24;
        display.drawLine(x,2,x,48,1);
        x=(display.width()/2)+24;
        display.drawLine(x,2,x,48,1);



        x=(display.width()/2)-(tuner_lastDiff*(display.width()/2));

        if(tuner_lastr&128) display.drawCircle(x,25,22,1);
        if((tuner_lastr&512)==0)
        {
            display.fillCircle(x,25,20,1);
            display.setTextColor(0,1);
        }
  else
  {
            display.fillCircle(x,25,19,0);
            display.drawCircle(x,25,20,1);
        }
        tnote=tuner_lastr&15;
        toffs=tnote*3;
        x-=(notelens[tnote]*4.51); // notenames[] lengths...
        if(x<0) x=0;
        if(x+(notelens[tnote]*12.2)>127) x=127-(notelens[tnote]*12.2);
        display.setTextSize(2);
        display.setCursor(x,18);
        display.printf("%s",&notenames[toffs]);
        display.setTextSize(1);
        if((tuner_lastr&512)==0) display.setTextColor(1,0);
        if(tuner_lastDetected>999)
        {
            display.setCursor(38,53);
        }
  else
        {
            if(tuner_lastDetected>99)
            {
                display.setCursor(43,53);
            }
   else
            {
     if(tuner_lastDetected>9)
    {
                    display.setCursor(48,53);
                }
    else
    {
                    display.setCursor(53,53);
                }
            }
        }
        display.print(tuner_lastDetected);
        display.print("Hz");
        display.display();
        dispflags&=~15;
        dispflags|=dispTuner;
}//void tunerDisplay()

float centerX(float left, float top, float width, float height, uint8_t len)
{
        return (left+(width/2))-(len*3);
}

void checkDisplay(uint8_t newMode)
{
        if((dispflags&15)==newMode) return; // we are already in the appropriate display mode.
        display.clearDisplay();
        switch(newMode)
  {
            case dispVols:
                display.setCursor(centerX(10,10,15,53,strlen((const char*)mCon.face_label)),0);
                display.printf("%s",mCon.face_label);
                display.setCursor(centerX(57,10,15,53,6),0);
                display.print("Volume");
                display.setCursor(centerX(103,10,15,53,strlen((const char*)mCon.mic_label)),0);
                display.printf("%s",mCon.mic_label);
                doDialPipsV(10,10,15,53,false);
                doDialPipsV(57,10,15,53,false);
                doDialPipsV(103,10,15,53,false);
                redoDialV(levs.b2f,0,1,10,10,15,53);
                redoDialV(levs.volume,0,1,57,10,15,53);
                redoDialV(levs.bf2m,0,1,103,10,15,53);
            break;
            case dispTone:
                display.setCursor(centerX(10,10,15,53,4),0);
                display.print("Bass");
                display.setCursor(centerX(57,10,15,53,8),0);
                display.print("Midrange");
                display.setCursor(centerX(103,10,15,53,6),0);
                display.printf("Treble");
                doDialPipsV(10,10,15,53,false);
                doDialPipsV(57,10,15,53,false);
                doDialPipsV(103,10,15,53,true);
                redoDialV(levs.bass,0,1,10,10,15,53);
                redoDialV(levs.mids,0,1,57,10,15,53);
                redoDialV(levs.treb,0,1,103,10,15,53);
            break;
            case dispLogo:
                display.drawBitmap(0,0,logo_data,128,64,1);
                display.setCursor(20,53);
                //    display.printf("(%u %u)",mSer.seryr,mSer.serhr);
                display.printf("ver:%u.%u.%u\n",1,2,2);
                //    display.printf("git:%s\n", GIT_VERSION);
            break;
            case dispLogo2:
                display.drawBitmap(0,0,logo2_data,128,64,1);
            break;
            case dispProfile:
                display.setTextSize(2);
                display.setCursor(18,10);
                if(profileMode) display.printf("User %u\nSettings",profileMode); else display.print("Factory\nSettings");
                display.setTextSize(1);
                display.setCursor(20,53);
                display.printf("ver:%u.%u.%u\n",1,2,2);
            break;

            default: // dispbatt.
                uint8_t n=batteryReading/mCon.battery_switch;
                if(n>1) n=1;

                display.setCursor(10,10);
                display.printf("Battery: %1.1fV",batteryReading);
                doDialPipsH(0,30,127,20);
                redoDialH(batteryReading,mCon.battery_dead[n],mCon.battery_max[n],0,30,127,20);
                display.setCursor(10,53);
                if(batteryReading<=mCon.battery_dead[n])
                {
                    display.print("Replace battery");
                }
       else
                {
        if(batteryReading<=mCon.battery_low[n])
                    {
                        display.print("Getting LOW!");
                    }
                }
        }
        dispflags&=~15;
        dispflags|=newMode;
        //  display.display(); - do this where actual changes occur instead.
} //    End  void checkDisplay(uint8_t newMode)

    //----------------------------------------------------------------------------- OLED  -------------------------------------------------------------------
void selectPreamp(bool wakeDisplay=true)
{

        digitalWrite(24,0);
        runLevel&=~2;
        ctrlMode=volctrls;
        if(wakeDisplay)
        {
            dispflags|=15;
            checkDisplay(ctrlMode);
            display.display();
            displayUp();
        }
        filterBass.frequency(mCon.svFilters[1].fC);
        filterBass.resonance(mCon.svFilters[1].Q);
        filterMidTreb.frequency(mCon.svFilters[2].fC);
        filterMidTreb.resonance(mCon.svFilters[2].Q);
        mixer_bfm.gain(0,exp(fmap(levs.bf2m,0,1,mCon.brfaLevel[0],mCon.brfaLevel[1])/10));
        mixer_bfm.gain(1,exp(fmap(levs.bf2m,0,1,mCon.micLevel[0],mCon.micLevel[1])/10));
        mixer_bfm.gain(2,0);
        mixer_tone.gain(0,actlevs[0].bass);
        mixer_tone.gain(1,actlevs[0].mids);
        mixer_tone.gain(2,actlevs[0].treb);
        adau1372.outputControl(true,false);

        applyVolume();
        mainMode=preamp;
} //    End  void selectPreamp(bool wakeDisplay=true)


void selectTuner(bool upDisp=true) // alias SelectMuting
{

        adau1372.outputControl(false,true);
        mixer_bfm.gain(0,0);
        mixer_bfm.gain(1,0);
        mixer_bfm.gain(2,1); // not silence!
        filterBass.frequency(mCon.tuningFilters[0].fC);
        filterBass.resonance(mCon.tuningFilters[0].Q);
        filterMidTreb.frequency(mCon.tuningFilters[1].fC);
        filterMidTreb.resonance(mCon.tuningFilters[1].Q);
        mixer_tone.gain(0,0);
        mixer_tone.gain(1,1);
        mixer_tone.gain(2,0);
        if(upDisp)
        {
            tuner_lastr=tuner_read;
            tunerDisplay();
            displayUp();
        }
        tuner_lastr=~tuner_read;
        digitalWrite(24,1);
        myTuner.flags|=2;

        actlevs[1].volume=exp(-120/10);
        runLevel|=2;
        mainMode=tuner;
} //    End  void selectTuner(bool upDisp=true)

        elapsedMillis battUpdate,profTimer;



void jackedIn(void)
{
        // connect left and middle buttons now.
        butMiddle.tappedCallback(butMiddleCycled);
        butSelect.tappedCallback(butSelectCycled);
        latchUp();
        runLevel&=~6; // clears 4 & 2, being tuner and unknown runLevels
        runLevel|=1;
        selectPreamp(false);
        dispflags|=15;
        checkDisplay(dispLogo);
        display.display();
        displayUp();
        display_timeout=5000 -3000;
} //    End  void jackedIn(void) 

void jackedOut(void)
{
        // disconnect left and middle buttons now, just in case ;)
        butMiddle.tappedCallback(
# 3300 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3 4
                                __null
# 3300 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                    );
        butSelect.tappedCallback(
# 3301 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3 4
                                __null
# 3301 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                    );


        runLevel&=~1;
        saveLastProfile();
        putPromData();
        saveLevs();
        //  displayUp();
} //    End  void jackedOut(void)



void butModeCycled(void)
{
        if(mainMode!=tuner||(runLevel&4)==4)
        {
            selectTuner();
        }
  else
  {
            if(runLevel&1)
            {
                selectPreamp();
            }
   else
   {
                mainMode=preamp;
                runLevel&=~2;
                checkDisplay(dispBatt);
                display.display();
                displayUp();
            }
        }
        runLevel&=~4;
} //    End  void butModeCycled(void)

void butSelectCycled(void)
{
        if(!(runLevel&~4)) return; // if runLevel is nothing or unknown then don't do this please :)
        if(butSelect.held>749)
        {
            putPromData();
            //profileMode=1-profileMode;
            if(++profileMode>=maxProfiles) profileMode=0;
            getPromData();
            Serial.printf("set profile %u\n",profileMode);
            dispflags|=15; // bugger up the display so that it shows the new profile even if immediately switching back...
            checkDisplay(dispProfile);
            display.display();
            displayUp();
            applyVolume();

        }
  else
  {
            //    if(dispflags&128)
            //{
            if(mainMode==tuner)
            {
                selectPreamp();
            }
   else
   {
                ctrlMode++;
                if(ctrlMode>tonectrls) ctrlMode=0;
                checkDisplay(ctrlMode);
                display.display();
                displayUp();
            }
            // }  else {
            //checkDisplay(ctrlMode);
            //displayUp();
            //}
        }
} //    End    void butSelectCycled(void)

void butMiddleCycled(void)
{
        checkDisplay(dispProfile);
        display.display();
        displayUp();
} //    End    void butMiddleCycled(void)
# 3417 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
/************************************************************************** SETUP *****************************************************************************/
void setup() {
/************************************************************************** SETUP *****************************************************************************/







        ((*((volatile uint32_t *)0xE000E180 + ((IRQ_SOFTWARE) >> 5)) = (1 << ((IRQ_SOFTWARE) & 31))));
        Wire.begin();
        ({ static __attribute__ ((section(".dmabuffers"), used)) audio_block_t data[25]; AudioStream::initialize_memory(data, 25); });
        Serial.begin(Serial.baud());
        Serial.println("Started Serial");


        pinMode(12,1);
        latchUp();
        runLevel|=4; // what started me?




        pinMode(29,1);
        digitalWrite(29,0);


        pinMode(20,1);
        digitalWrite(20,1);
        display.begin(0x2, 0x3D); // initialize with the I2C addr 0x3D (for the 128x64)
        display.setTextSize(1);
        display.setTextColor(1,0);
        display.ssd1306_command(0xAE);
        dispflags&=~128;
        /******************************************************************* TUNER REFERENCE *******************/
        pinMode(24,1);
        pinMode(11,0);

        Serial.println("Attaching interupt on PIN_SLAPPER_OUTPUT");
        attachInterrupt(11,slapper_isr,4);
        (*((volatile uint8_t *)0xE000E400 + (IRQ_PORTC)) = (uint8_t)(4));
        myTuner.flags|=1; // '1' == debug output to console.





        pinMode(7,2);
        (*(volatile uint32_t *)0x400BB020) /* Status and control register 2*/ = ((uint32_t)(((0) & 3) << 0)) /* Voltage reference, 0=vcc/external, 1=1.2 volts*/;

        pinMode(31,2);
        butSelect.tappedCallback(
# 3469 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3 4
                                __null
# 3469 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                    ); // comment this line out some time, it isn't actually doing anything.
        butSelect.longPressMillis=750;

        butMode.longPressMillis=750;

        pinMode(28,2);
        butJack.pressCallback(jackedIn);
        butJack.releaseCallback(jackedOut);

        pinMode(15,2);
        butMiddle.tappedCallback(
# 3479 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino" 3 4
                                __null
# 3479 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
                                    ); // comment this line out some time, ...
        butMiddle.longPressMillis=750;

        adc->setResolution(12,1);
        adc->setAveraging(32,1); // previously 32
        adc->setSamplingSpeed(2,1); // previously ADC_MED_SPEED
        adc->setConversionSpeed(2,1); // previously ADC_MED_SPEED





            uint8_t adc_current=0;



            while(tie_downs[adc_current]!=255) { pinMode(tie_downs[adc_current],1); digitalWrite(tie_downs[adc_current++],0); }




        batteryReading=9;

        butMode.tappedCallback(butModeCycled);
        delay(50);

        adau1372.enableSlave(29);
        adau1372.outputControl(false,false);
        ((*((volatile uint32_t *)0xE000E100 + ((IRQ_SOFTWARE) >> 5)) = (1 << ((IRQ_SOFTWARE) & 31))));

    /* #ifdef HAS_ENCODERS
        maxProfiles=(E2END-(4+sizeof(mSer)))/(sizeof(mCon)+2); // EEPROM packing scheme includes 1-2 byte gaps between used data blocks.
    #else */
        maxProfiles=(0xFFF -(4+sizeof(mSer)+sizeof(levs)))/(sizeof(mCon)+2); // EEPROM packing scheme includes 1-2 byte gaps between used data blocks.
        if(maxProfiles>4) maxProfiles=4;
    // #endif

        mSer=getSerialData();
        profileMode=returnLastProfile();
        getPromData();
        getLevs();
# 3529 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
    //    adau1372.outputControl(true,false);


        ctrlMode=volctrls;
        display_timeout=0;
        adc->startSingleRead(A10,1);




        Serial.println("I made it to the end of setup.");







/************************************************************************** SETUP *****************************************************************************/
} // End of Setup()
/************************************************************************** SETUP *****************************************************************************/

uint8_t cur_check=0,cur_ramp=0;


//*************************************************************************************************************************************************************
void loop() {
//*************************************************************************************************************************************************************
// Added by Simon to pulse the Blue LED once a second
# 3600 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        // ***************************************************
        if(Serial.available()) rcvr.push(Serial.read());
        butMode.update();

            butSelect.update();
            butJack.update();
            butMiddle.update();
            if(battUpdate>500)
            {
                if(adc->isComplete(1))
                {
                    // batteryReading=(((float)adc->readSingle(ADC_1)/4095)*3.3)*VBRATIO;
                    batteryReading=0.2f +fmap(adc->readSingle(1),0,adc->getMaxValue(1),0,3.3)*((33000)+(68000))/(33000);

                    adc->startSingleRead(A10,1);
                }
            }


            tuner_read=myTuner.update();
            if(runLevel==4)
            {
                if(display_timeout>1000)
                {
                    runLevel&=~4; // clear unknown start bit.

                        if(robs_early_detection_flags&robs_early_tuner_flag)

                    selectTuner(); // if there is no early detection of that button then defaulting to Tuner mode will have to do.
                }
            }

            if(dispflags&128)
            {
                if(display_timeout>5000)
                {
                    // Turn off display
                    displayDown();
                    if(!runLevel)
                    {
                        latchDown();
                    }
                }
                else
                {
                    if(((dispflags&15)!=dispBatt)&&((dispflags&15)!=dispLogo)&&((dispflags&15)!=dispLogo2))
                    {
                        uint8_t n=batteryReading/mCon.battery_switch;
                        if(n>1) n=1;
                        if((display_timeout>5000*0.8)||((display_timeout>5000*0.4)&&(batteryReading<mCon.battery_low[n])))
                        {
                            checkDisplay(dispBatt);
                            display.display();
                        }
                    }
                }
            }

        if(runLevel)
        {
            if(mainMode==preamp)
            {

                    if(encoder_pace>50)
                    {
                        encoder_pace=0;
                        int encLft=encL.read(); //ENCVALUESPERCLICK;
                        encL.write(0);
                        int encMid=encM.read(); //ENCVALUESPERCLICK;
                        encM.write(0);
                        int encRgt=encR.read(); //ENCVALUESPERCLICK;
                        encR.write(0);
                        encLft=({ typeof(encLft) _x = (encLft); (_x > 0) ? _x : -_x; })*encLft;
                        encRgt=({ typeof(encRgt) _x = (encRgt); (_x > 0) ? _x : -_x; })*encRgt;
                        encMid=({ typeof(encMid) _x = (encMid); (_x > 0) ? _x : -_x; })*encMid;

                        if(encLft+encMid+encRgt!=0)
                        {
                            checkDisplay(ctrlMode);
                            switch(ctrlMode)
                            {
                                case volctrls:
                                    if(encLft) // bridge/face
                                    {
                                        levs.b2f+=(float)encLft/300;
                                        if(levs.b2f>1) levs.b2f=1;
                                        if(levs.b2f<0) levs.b2f=0;
                                        applyB2f();

                                            redoDialV(levs.b2f,0,1,10,10,15,53);

                                        // Serial.printf("bridge/face %f\n",mCon.b2f);
                                    }
                                    if(encMid) // Volume
                                    {
                                        levs.volume+=(float)encMid/300;
                                        if(levs.volume>1) levs.volume=1;
                                        if(levs.volume<0) levs.volume=0;
                                        applyVolume();

                                           redoDialV(levs.volume,0,1,57,10,15,53);

                                    }
                                    if(encRgt) // bridge+face/mic
                                    {
                                        levs.bf2m+=(float)encRgt/300;
                                        if(levs.bf2m>1) levs.bf2m=1;
                                        if(levs.bf2m<0) levs.bf2m=0;
                                        applyBf2m();

                                            redoDialV(levs.bf2m,0,1,103,10,15,53);

                                    }
                                break;
                                // case tonectrls:
                                default:
                                    if(encLft) // Bass
                                    {
                                        levs.bass+=(float)encLft/300;
                                        if(levs.bass>1) levs.bass=1;
                                        if(levs.bass<0) levs.bass=0;
                                        applyBass();

                                            redoDialV(levs.bass,0,1,10,10,15,53);

                                    }
                                    if(encMid) // Midrange
                                    {
                                        levs.mids+=(float)encMid/300;
                                        if(levs.mids>1) levs.mids=1;
                                        if(levs.mids<0) levs.mids=0;
                                        applyMids();

                                            redoDialV(levs.mids,0,1,57,10,15,53);

                                    }
                                    if(encRgt) // Treble
                                    {
                                        levs.treb+=(float)encRgt/300;
                                        if(levs.treb>1) levs.treb=1;
                                        if(levs.treb<0) levs.treb=0;
                                        applyTreb();

                                            redoDialV(levs.treb,0,1,103,10,15,53);

                                    }
                            }

                                display.display();
                                displayUp();

                        }
                    }
# 3890 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
            }
            else
            { // two modes atm, this is either muted or it is tuning mode.
                if(peak_bridge.available()&&encoder_pace>20)
                {
                    encoder_pace=0;
                    float nn=peak_bridge.read();
                    //float nnl=myTuner.gain;
                    if(nn>0.0012)
                    mixer_bfm.gain(2,0.6/nn);
                }
                else
                {
                    mixer_bfm.gain(2,0.6);
                }

                if((tuner_read!=tuner_lastr||tuner_lastDetected!=myTuner.detectedFreq)&&5000>149)
                {
                    // if(myTuner.detectedFreq!=0||display_timeout>DISPLAY_TIMEOUT/2)
                    if(myTuner.detectedFreq!=0||display_timeout>5000/4)
                    {
                        tuner_lastr=tuner_read;
                        tuner_lastDiff=myTuner.lastDiff;
                        tuner_lastDetected=myTuner.detectedFreq;
                        tunerDisplay();
                        displayUp();
                    }
                }
                if((runLevel&1)==0) if(display_timeout>30000) runLevel&=~2; // selectPreamp();

            }

            // RAMPING BLOCK OF CODE WAS HERE!
            /*** Ramping happens here, outside those blocks for now. ***/
            //    if(mic_pass1.acted()&&mainMode==preamp)
            if(bass_pass.acted())
            {
                float tempVal=0.025; // dB2ratio(mCon.rampRate);
                switch(cur_ramp)
                {
                    case 0: // volume
                        if(actlevs[0].volume<actlevs[1].volume)
                        {
                            if(actlevs[0].volume+tempVal<actlevs[1].volume)
                            {
                                actlevs[0].volume+=tempVal;
                            }
                            else
                            {
                                actlevs[0].volume=actlevs[1].volume;
                            }
                            mixer_final.gain(0,actlevs[0].volume);
                        }
                        else
                        {
                            if(actlevs[0].volume>actlevs[1].volume)
                            {
                                if(actlevs[0].volume-tempVal>actlevs[1].volume)
                                {
                                    actlevs[0].volume-=tempVal;
                                }
                                else
                                {
                                    actlevs[0].volume=actlevs[1].volume;

                                }
                                mixer_final.gain(0,actlevs[0].volume);
                            }
                        }
                    break;
                    case 1: // bass_level
                        if(actlevs[0].bass<actlevs[1].bass)
                        {
                            if(actlevs[0].bass+tempVal<actlevs[1].bass)
                            {
                                actlevs[0].bass+=tempVal;
                            }
                            else
                            {
                                actlevs[0].bass=actlevs[1].bass;
                            }
                            mixer_tone.gain(0,actlevs[0].bass);
                        }
                        else
      {
                            if(actlevs[0].bass>actlevs[1].bass)
                            {
                                if(actlevs[0].bass-tempVal>actlevs[1].bass)
                                {
                                    actlevs[0].bass-=tempVal;
                                }
                                else
                                {
                                    actlevs[0].bass=actlevs[1].bass;

                                }
                                mixer_tone.gain(0,actlevs[0].bass);
       }
                        }
                    break;
                    case 2: // mids_level
                        if(actlevs[0].mids<actlevs[1].mids)
                        {
                            if(actlevs[0].mids+tempVal<actlevs[1].mids)
                            {
                                actlevs[0].mids+=tempVal;
                            }
                            else
                            {
                                actlevs[0].mids=actlevs[1].mids;
                            }
                            mixer_tone.gain(1,actlevs[0].mids);
                        }
                        else
                        {
                            if(actlevs[0].mids>actlevs[1].mids)
                            {
                                if(actlevs[0].mids-tempVal>actlevs[1].mids)
                                {
                                    actlevs[0].mids-=tempVal;
                                }
                                else
                                {
                                    actlevs[0].mids=actlevs[1].mids;
                                }
                                mixer_tone.gain(1,actlevs[0].mids);
                            }
                        }
                    break;
                    case 3: // treb_level
                        if(actlevs[0].treb<actlevs[1].treb)
                        {
                            if(actlevs[0].treb+tempVal<actlevs[1].treb)
                            {
                                actlevs[0].treb+=tempVal;
                            }
                            else
                            {
                                actlevs[0].treb=actlevs[1].treb;
                            }
                            mixer_tone.gain(2,actlevs[0].treb);
                        }
                        else
      {
                            if(actlevs[0].treb>actlevs[1].treb)
                            {
                                if(actlevs[0].treb-tempVal>actlevs[0].treb)
                                {
                                    actlevs[0].treb-=tempVal;
                                }
                                else
                                {
                                    actlevs[0].treb=actlevs[1].treb;

                                }
                                mixer_tone.gain(2,actlevs[0].treb);
                            }
                        }
                }
                cur_ramp++;
                if(cur_ramp>3) cur_ramp=0;
            }
# 4103 "W:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\DGPreamp_1813\\DGPreamp_1813.ino"
        }
        else
        {
            if((dispflags&192)==64)
            {
               checkDisplay(dispBatt);
               display.display();
               displayUp();
            }

  }

// ***************************************************************************************************************************************************
} // void loop()
// ***************************************************************************************************************************************************




//  #warning Sample Rate Appears OK.
