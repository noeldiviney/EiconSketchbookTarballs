#include <time.h>

// Pins association 
const int Pot_Read_Pin_1 = A0;  // Analog input
const int Pot_Read_Pin_2 = A1;
const int Pot_Read_Pin_3 = A2;
const int Pwm_Out_Pin_1  = 13;  // PWM output for LEDa
const int Pwm_Out_Pin_2  = PB4; 
const int Pwm_Out_Pin_3  = PB5; 
const int Dig_In_Pin_1   = 1;   // Digital input
const int Dig_In_Pin_2   = 2; 
const int Dig_In_Pin_3   = 3; 
const int Dig_Out_Pin_1  = 4;   // Digital output
const int Dig_Out_Pin_2  = 5; 
const int Dig_Out_Pin_3  = 6; 
const int Dig_Out_Pin_4  = 7; 

// Global Variables
int Pot_Read_1  = 0;        
int Pot_Read_2  = 0;        
int Pot_Read_3  = 0;        
int Pwm_Out_1   = 0;        
int Pwm_Out_2   = 0;        
int Pwm_Out_3   = 0;        
int Dig_Out_1 = HIGH;
int Dig_Out_2 = HIGH;
int Dig_Out_3 = HIGH;
int Dig_Out_4 = HIGH;
unsigned long start;
unsigned long now;

// Average Function
int average_adc(int avg_number,int analog_pin){
  int buffer_array[128];
  int long calculated = 0;
  int i=0;   
  for (i = 0; i<avg_number; i++){      
    buffer_array[i] = analogRead(analog_pin);
    calculated += buffer_array[i];
//    delay(2);
  }
  calculated = calculated/avg_number;
  return (calculated);
}

// SETUP
void setup() {  
  // Initialize the Serial port
  Serial_begin(9600);
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s("Begin. \r\n");

  // Get the start time
  start = micros();
}

// LOOP
void loop() {    
//  Pot_Read_1 = analogRead(Pot_Read_Pin_1); //Reads adc
//  Pot_Read_2 = analogRead(Pot_Read_Pin_2);
//  Pot_Read_3 = analogRead(Pot_Read_Pin_3);
  Pot_Read_1 = average_adc(128,Pot_Read_Pin_1); //Reads adc and returns its average
  Pot_Read_2 = average_adc(128,Pot_Read_Pin_2);
  Pot_Read_3 = average_adc(128,Pot_Read_Pin_3);

  Pwm_Out_1 = map(Pot_Read_1, 0, 1023, 0, 255); //Range transform from 1023 to 255
  Pwm_Out_2 = map(Pot_Read_2, 0, 1023, 0, 255);
  Pwm_Out_3 = map(Pot_Read_3, 0, 1023, 0, 255);

  if(Pot_Read_1 <8) //Protection for <1% cicle 
    Pwm_Out_1 = 1;  //Send PWM with close to 1% Duty Cycle
  if(Pot_Read_2 <8)
    Pwm_Out_2 = 1;
  if(Pot_Read_3 <8)
    Pwm_Out_3 = 1;

  if(Pot_Read_1>1020) //Protection for >99% cicle
    Pwm_Out_1 = 254; //Send PWM with close to 99% Duty Cycle
  if(Pot_Read_2>1020) //Protection for >99% cicle
    Pwm_Out_2 = 254;
  if(Pot_Read_3>1020)
    Pwm_Out_3 = 254;

  analogWrite(Pwm_Out_Pin_1, Pwm_Out_1);
  analogWrite(Pwm_Out_Pin_2, Pwm_Out_2);
  analogWrite(Pwm_Out_Pin_3, Pwm_Out_3);

  // Read Digital input Pins
  Dig_Out_1 = digitalRead(Dig_In_Pin_1);
  Dig_Out_2 = digitalRead(Dig_In_Pin_2);
  Dig_Out_3 = digitalRead(Dig_In_Pin_3);

  // Write Digital output pins
  digitalWrite(Dig_Out_Pin_1, Dig_Out_1);
  digitalWrite(Dig_Out_Pin_2, Dig_Out_2);
  digitalWrite(Dig_Out_Pin_3, Dig_Out_3);

  // Blink the 4th digital output pin
  if (Dig_Out_4 == HIGH)
    Dig_Out_4 = LOW;
  else
    Dig_Out_4 = HIGH;
  digitalWrite(Dig_Out_Pin_4, Dig_Out_4);

  // Output some serial info
  Serial_print_s("D1:");
  Serial_print_u(Dig_Out_1);
  Serial_print_s(" D2:");
  Serial_print_u(Dig_Out_2);
  Serial_print_s(" D3:");
  Serial_print_u(Dig_Out_3);
  Serial_print_s(" D4:");
  Serial_print_u(Dig_Out_4);

  Serial_print_s("  A1:");
  Serial_print_u(Pot_Read_1);
  Serial_print_s(" A2:");
  Serial_print_u(Pot_Read_2);
  Serial_print_s(" A3:");
  Serial_print_u(Pot_Read_3);

  Serial_print_s("  PWM1:");
  Serial_print_u(Pwm_Out_1);
  Serial_print_s(" PWM2:");
  Serial_print_u(Pwm_Out_2);
  Serial_print_s(" PWM3:");
  Serial_print_u(Pwm_Out_3);

  Serial_print_s("  Time:");
  now = micros();
  if ((now - start) / 100000 > 99)
    start = now;
  Serial_print_u((now - start) / 100000);
  Serial_print_s(" \r\n");

//  delay(10);
}
