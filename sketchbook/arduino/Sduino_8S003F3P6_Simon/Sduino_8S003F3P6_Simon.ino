#include <time.h>

//----------------------------------
// Pins mapping
//----------------------------------

// Digital Input 
const int DigInPin1                = PC3;  // Up button
const int DigInPin2                = PC5;  // Down Button 
const int DigInPin3                = PC4;  // Start-Stop button

// Digital Output
const int DigInPinDigOutBlinkPin1 = PB4;  // LED 1
const int DigInPinDigOutBlinkPin2 = PB5;  // LED 2
const int DigOutHeaterRelayPin1   = PC6;  // Heater Relay 1
const int DigOutHeaterRelayPin2   = PD3;  // Heater Relay 2

// Display
const int DigOutDispPin1          = PA1; // CLK
const int DigOutDispPin2          = PA2; // STB
const int DigOutDispPin3          = PA3; // DIO


//----------------------------------
// Global Variables
//----------------------------------

// States, Rates and Counts
unsigned int DigIn1       = HIGH;
unsigned int DigIn2       = HIGH;
unsigned int DigIn3       = HIGH;

unsigned int DigInPrev1   = HIGH;
unsigned int DigInPrev2   = HIGH;
unsigned int DigInPrev3   = HIGH;

unsigned int DigInCount1  = 0;
unsigned int DigInCount2  = 0;
unsigned int DigIn3_Count = 0;

unsigned int DigInRate1   = 200;
unsigned int DigInRate2   = 200;   
unsigned int DigInRate3   = 100;    

unsigned int DigInState3  = false;

unsigned int blinkState   = 0;

// Schedule Timing
unsigned long now               =  0;
unsigned long start             =  0;

unsigned long lastDigInHop      =  0;
unsigned long lastDigOutHop     =  0;
unsigned long lastSerialInHop   =  0;
unsigned long lastSerialOutHop  =  0;
unsigned long lastTimerHop      =  0;
unsigned long lastBlinkHop      =  0;

unsigned long durationDigIn     = 0.03  * 1000000;
unsigned long durationDigOut    = 0.1   * 1000000;
unsigned long durationSerialIn  = 0.1   * 1000000;
unsigned long durationSerialOut = 1.0   * 1000000;
unsigned long durationTimer     = 1.0   * 1000000;
unsigned long durationBlink     = 0.5   * 1000000;

// Timer Status
unsigned long timerStartTime    = 0;
unsigned long timerStopTime     = 0;
unsigned long timerFullTime     = 15;
unsigned int  timerState        = 0;
unsigned int  timerState2       = 0;

// Display
const unsigned int strobe = PA2;
const unsigned int clock  = PA1;
const unsigned int data   = PA3;
const unsigned int segA[10] = {0xfe, 0x02, 0x1d, 0x57, 0x03, 0x17, 0x1f, 0x72, 0xff, 0x73};
const unsigned int segB[10] = {0xfe, 0x13, 0x57, 0x57, 0x19, 0x48, 0x48, 0x72, 0xff, 0x19};
const unsigned int digA[4] = {0xc0, 0xc2, 0xc4, 0xc6};
const unsigned int digB[4] = {0xc1, 0xc3, 0xc5, 0xc7};

// Serial processing
char   charArray[32];

// Talk to display
void displaySendCommand(uint8_t value)
{
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, value);
  digitalWrite(strobe, HIGH);
}

// Reset the display
void displayReset()
{
  displaySendCommand(0x40); // set auto increment mode
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, 0xc0);   // set starting address to 0
  for(uint8_t i = 0; i < 16; i++)
  {
    shiftOut(data, clock, LSBFIRST, 0x00);
  }
  digitalWrite(strobe, HIGH);
}

// Update a digit on the display
void displayUpdateDigit(unsigned int location, unsigned int digit){
  displaySendCommand(0x44);  // set single address
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, digA[location]);
  shiftOut(data, clock, LSBFIRST, segA[digit]);
  digitalWrite(strobe, HIGH);
  digitalWrite(strobe, LOW);
  shiftOut(data, clock, LSBFIRST, digB[location]);
  shiftOut(data, clock, LSBFIRST, segB[digit]);
  digitalWrite(strobe, HIGH);
  digitalWrite(PB4, HIGH);
  digitalWrite(PB5, LOW);
}

// Decriment the heater runtime event
void processTimerUpEvent(){
  if (timerFullTime <= (30 * 60 - 1))
    timerFullTime++;
  processDisplayUpdateEvent();
//  processSerialOutEvent();
}

// Incriment the heater runtime event
void processTimerDownEvent(){
  if (timerFullTime >= 1)
    timerFullTime--;

  processDisplayUpdateEvent();
//  processSerialOutEvent();
}

// Start the Heater Event
void processTimerStartEvent(){
  if (timerState == 0){
    if (timerFullTime > 0){
      Serial_print_s("Starting Timer. \r\n");
      timerState = 1;
      timerStartTime = now;
    }
    else{
      Serial_print_s("NOT Starting Timer. Need to set time! \r\n");
    }
  }
  else if (timerState == 1){
    Serial_print_s("Stopping Timer. \r\n");
    timerState = 0; 
    timerStopTime = now;
  }
}

// Process Serial Input Event
void processSerialInEvent() {
  while (Serial_available() > 0) {
    // read incoming serial data:
    char inChar = Serial_read();
    delay(10);
    if (inChar == '\n'){
      Serial_print_s("CR received! \r\n");
    }
    else{
      Serial_print_s("Received:["); Serial_print_c(inChar); Serial_print_s("] \r\n");
      if (inChar == '1'){
        Serial_print_s("Command:[1-UP] \r\n");
        processTimerUpEvent();
      }
      else if (inChar == '2'){
        Serial_print_s("Command:[2-DOWN] \r\n");
        processTimerDownEvent();
      }
      else if (inChar == '3'){
        Serial_print_s("Command:[3-START] \r\n");
        processTimerStartEvent();
      }
      else{
        Serial_print_s("Command:["); Serial_print_c(inChar); Serial_print_s("-UNKNOWN!] \r\n");
      }
    }
  }
}

// Process Display Update event
void processDisplayUpdateEvent(){
  // Update the display
  displayUpdateDigit(0, (timerFullTime / 60 / 10) );
  displayUpdateDigit(1, (timerFullTime / 60 % 10) );
  displayUpdateDigit(2, (timerFullTime % 60 / 10) );
  displayUpdateDigit(3, (timerFullTime % 60 % 10) );
}

// Output some serial ststus
void processSerialOutEvent() {
  Serial_print_s("Up Time:");
  sprintf(charArray, "%02ld:%02ld", (((now - start) / 1000000) / 60), (((now - start) / 1000000) % 60));
  Serial_print_s(charArray);
  Serial_print_s("   ");

  Serial_print_s("Heater Time:");
  sprintf(charArray, "%02ld:%02ld", (timerFullTime / 60), (timerFullTime % 60));
  Serial_print_s(charArray);
  Serial_print_s("   ");

  Serial_print_s("Heater:");
  if (timerState == 1)
    Serial_print_s("[ON] ");
  else
    Serial_print_s("[OFF]");
  Serial_print_s("   ");
    
  Serial_print_s(" \r\n");

  now = micros();
}

// Read the state of the touch buttons
void processDigInEvent() {
  // Store the old values for comparison
  DigInPrev1 = DigIn1;
  DigInPrev2 = DigIn2;
  DigInPrev3 = DigIn3;

  // Read Digital input Pins
  DigIn1 = digitalRead(DigInPin1);
  DigIn2 = digitalRead(DigInPin2);
  DigIn3 = digitalRead(DigInPin3);

  // Button 1 Timer-UP
  if (DigIn1 == LOW){
    if (DigInPrev1 == HIGH){
//      Serial_print_s("Button:[1-UP] PRESS\r\n");
      processTimerUpEvent();
    }      
    DigInCount1++;
    if (DigInCount1 >= DigInRate1){
//      Serial_print_s("Button:[1-UP] HOLD\r\n");
      processTimerUpEvent();
      DigInCount1 = 0;
      if (DigInRate1 > 0){
        DigInRate1--;
      }
    }
  }
  else{
    DigInCount1 = 0;
    DigInRate1 = 10;
  }
  
  // Button 2 Timer-DOWN
  if (DigIn2 == LOW){
    if (DigInPrev2 == HIGH){
//      Serial_print_s("Button:[2-DOWN] PRESS\r\n");
      processTimerDownEvent();
    }      
    DigInCount2++;
    if (DigInCount2 >= DigInRate2){
//      Serial_print_s("Button:[2-DOWN] HOLD\r\n");
      processTimerDownEvent();
      DigInCount2 = 0;
      if (DigInRate2 > 0){
        DigInRate2--;
      }
    }
  }
  else{
    DigInCount2 = 0;
    DigInRate2 = 10;
  }

  // Button 3 Timer-STOP-START
  if (DigIn3 == LOW){
    DigIn3_Count++;
    if ((DigIn3_Count >= DigInRate3) && (DigInState3 == false)){
//      Serial_print_s("Button:[3-START-STOP] HOLD\r\n");
      DigInState3 = true;
      processTimerStartEvent();
      DigIn3_Count = 0;
      if (DigInRate3 > 0){
        DigInRate3--;
      }
    }
  }
  else{
    DigInState3 = false;
    DigIn3_Count = 0;
    DigInRate3 = 10;
  }

}

// Write DIO {turn relays ON / OFF}
void processDigOutEvent() {
  digitalWrite(DigOutHeaterRelayPin1, timerState);
  digitalWrite(DigOutHeaterRelayPin2, timerState2);
}

// Blink the 2 LEDs
void processBlinkEvent(){
  if (blinkState==HIGH)
    blinkState = LOW;
  else
    blinkState = HIGH;
  digitalWrite(DigInPinDigOutBlinkPin1, blinkState);
  digitalWrite(DigInPinDigOutBlinkPin2, !blinkState);
}

// Update timer status'
void processTimerEvent() {
// Update the display
  processDisplayUpdateEvent();

// Turn the first relay OFF if the timer has run down
  if (timerState == 1){
    timerFullTime--;
    if (timerFullTime <= 0){
      timerState = 0;
      timerStopTime = now;
    }
  }

// Turn the second relay ON or OFF one second after the first relay
  if (timerState == 1){
    if (now - timerStartTime > 1000000)
      timerState2 = 1;
  }
  else{
    if (now - timerStopTime > 1000000)
      timerState2 = 0;
  }
  
}

// Write the Pinout info to the serial console
/*
void serialOutputPinOuts(){
  Serial_print_s("PD4:");  sprintf(charArray, "%02d \r\n", PD4);  Serial_print_s(charArray);
  Serial_print_s("PD5:");  sprintf(charArray, "%02d \r\n", PD5);  Serial_print_s(charArray);
  Serial_print_s("PD6:");  sprintf(charArray, "%02d \r\n", PD6);  Serial_print_s(charArray);
  Serial_print_s("NRST: \r\n");
  Serial_print_s("PA1:");  sprintf(charArray, "%02d \r\n", PA1);  Serial_print_s(charArray);
  Serial_print_s("PA2:");  sprintf(charArray, "%02d \r\n", PA2);  Serial_print_s(charArray);
  Serial_print_s("GND: \r\n");
  Serial_print_s("VCAP: \r\n");
  Serial_print_s("VDD: \r\n");
  Serial_print_s("PA3:");  sprintf(charArray, "%02d \r\n", PA3);  Serial_print_s(charArray);

  Serial_print_s("PB5:");  sprintf(charArray, "%02d \r\n", PB5);  Serial_print_s(charArray);
  Serial_print_s("PB4:");  sprintf(charArray, "%02d \r\n", PB4);  Serial_print_s(charArray);
  Serial_print_s("PC3:");  sprintf(charArray, "%02d \r\n", PC3);  Serial_print_s(charArray);
  Serial_print_s("PC4:");  sprintf(charArray, "%02d \r\n", PC4);  Serial_print_s(charArray);
  Serial_print_s("PC5:");  sprintf(charArray, "%02d \r\n", PC5);  Serial_print_s(charArray);
  Serial_print_s("PC6:");  sprintf(charArray, "%02d \r\n", PC6);  Serial_print_s(charArray);
  Serial_print_s("PC7:");  sprintf(charArray, "%02d \r\n", PC7);  Serial_print_s(charArray);
  Serial_print_s("PD1:");  sprintf(charArray, "%02d \r\n", PD1);  Serial_print_s(charArray);
  Serial_print_s("PD2:");  sprintf(charArray, "%02d \r\n", PD2);  Serial_print_s(charArray);
  Serial_print_s("PD3:");  sprintf(charArray, "%02d \r\n", PD3);  Serial_print_s(charArray);
}
*/

void setup() {  
  // Initialize the Serial port
  Serial_begin(9600);
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s("-------------------------------------------------------------------- \r\n");
  delay(50);
  Serial_print_s("Begin. \r\n");
  delay(50);
  Serial_print_s("1 = [TIMER-UP]    2 = [TIMER-DOWN]    3 = [START / STOP] \r\n");
  delay(50);
  Serial_print_s("-------------------------------------------------------------------- \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(50);
  Serial_print_s(" \r\n");
  delay(500);

  pinMode(DigInPin1, INPUT_PULLUP);
  pinMode(DigInPin2, INPUT_PULLUP);
  pinMode(DigInPin3, INPUT_PULLUP);

  pinMode(DigInPinDigOutBlinkPin1, OUTPUT);
  pinMode(DigInPinDigOutBlinkPin2, OUTPUT);

  pinMode(DigOutHeaterRelayPin1, OUTPUT);
  pinMode(DigOutHeaterRelayPin2, OUTPUT);

  pinMode(strobe, OUTPUT);
  pinMode(clock, OUTPUT);
  pinMode(data, OUTPUT);

  displaySendCommand(0x8f);  // activate
  displayReset();
  
  // Get the start time
  start = micros();

  now = micros();
  start            = now;
  lastDigInHop     = now;
  lastDigOutHop    = now;
  lastSerialInHop  = now;
  lastSerialOutHop = now;
  lastTimerHop     = now;
  lastBlinkHop     = now;
}

void loop() {  
  if (now - lastTimerHop >= durationTimer){
    lastTimerHop = now;
    processTimerEvent();
  }
  if (now - lastSerialOutHop >= durationSerialOut){
    lastSerialOutHop = now;
    processSerialOutEvent();
  }
  if (now - lastSerialInHop >= durationSerialIn){
    lastSerialInHop = now;
    processSerialInEvent();
  }
  if (now - lastDigInHop >= durationDigIn){
    lastDigInHop = now;
    processDigInEvent();
  }
  if (now - lastDigOutHop >= durationDigOut){
    lastDigOutHop = now;
    processDigOutEvent();
  }
  if (now - lastBlinkHop >= durationBlink){
    lastBlinkHop = now;
    processBlinkEvent();
  }

  now = micros();
}
